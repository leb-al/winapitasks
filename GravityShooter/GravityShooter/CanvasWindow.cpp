#include "stdafx.h"
#include "CanvasWindow.h"
#include "OverlappedWindow.h"
#include "SpaceObjects.h"

CCanvasWindow::CCanvasWindow(CModel * model) : model(model)
{
	bitmap = 0;
	bitmapContext = 0;
	bitmapWidth = 1;
	bitmapHeight = 1;
	centerX = 0;
	centerY = 0;
	leftPressed = false;
	rightPressed = false;
}

CCanvasWindow::CCanvasWindow() : CCanvasWindow(0)
{
}

CCanvasWindow::~CCanvasWindow()
{
}

bool CCanvasWindow::RegisterClass()
{
	WNDCLASSEX windowClassInforamtion;

	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);

	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 2 * sizeof(LONG_PTR);
	HMODULE instance = GetModuleHandleW(0);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = 0;
	windowClassInforamtion.hCursor = LoadCursor(0, IDC_CROSS);
	windowClassInforamtion.hbrBackground = 0;
	windowClassInforamtion.lpszMenuName = 0;
	windowClassInforamtion.lpszClassName = CCanvasWindow::ClassName;
	windowClassInforamtion.hIconSm = 0;

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}


bool CCanvasWindow::Create(HWND parent)
{
	handle = CreateWindowEx(0, CCanvasWindow::ClassName, L"Canvas window for gravity shooter", WS_CHILDWINDOW,
		0, 0, 500, 400, parent, 0, GetModuleHandleW(nullptr), this);
	createResouces();
	return (handle != 0);
}

void CCanvasWindow::Show(int cmdShow)
{
	ShowWindow(handle, cmdShow);
	timer = SetTimer(handle, DrawTimerID, 10, 0);
	if (timer == 0) {
		::MessageBox(handle, L"No timer :(", L"Error", MB_ICONERROR);
		PostQuitMessage(5);
	}
}

HWND CCanvasWindow::GetHandle()
{
	return handle;
}

void CCanvasWindow::SetModel(CModel* model)
{
	this->model = model;
}

void CCanvasWindow::OnDestroy()
{
	KillTimer(handle, timer);
	clearResources();
	destroyDoubleBuffer();
}

const wchar_t* CCanvasWindow::ClassName = L"CCanvasWindow";
const int CCanvasWindow::DrawTimerID = 1;
const int CCanvasWindow::RocketImageSize = 32;
const int CCanvasWindow::FireWidth = 10;
const int CCanvasWindow::FireLength = 40;

LRESULT __stdcall CCanvasWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLongPtr(handle, 0, reinterpret_cast<LONG_PTR>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	CCanvasWindow* wndPtr = reinterpret_cast<CCanvasWindow*>(GetWindowLongPtr(handle, 0));
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_PAINT:
			wndPtr->onPaint();
			return 0;
		case WM_TIMER:
			wndPtr->drawTimerTick();
			return 0;
		case WM_SIZING:
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->resize(reinterpret_cast<RECT*>(lParam));
			return result;
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_LBUTTONDOWN:
			wndPtr->leftPressed = true;
			wndPtr->setMoveAccelerations(lParam);
			break;
		case WM_MOUSEMOVE:
			if (wndPtr->leftPressed) {
				wndPtr->setMoveAccelerations(lParam);
			}
			break;
		case WM_MOUSELEAVE:
			wndPtr->stopMoving(lParam);
			break;
		case WM_LBUTTONUP:
			wndPtr->stopMoving(lParam);
			break;
		case WM_RBUTTONDOWN:
			wndPtr->onRightButton(lParam);
			break;
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void CCanvasWindow::onPaint()
{
	PAINTSTRUCT paintStruct;
	HDC paintDC = ::BeginPaint(handle, &paintStruct);

	RECT rect;
	::GetClientRect(handle, &rect);

	drawContent(bitmapContext, bitmapWidth, bitmapHeight);
	BOOL code = BitBlt(paintDC, 0, 0, bitmapWidth, bitmapHeight, bitmapContext, 0, 0, SRCCOPY);
	if (!code) {
		::MessageBox(handle, L"Bad paint", L"DEBUG", MB_ICONERROR);
		PostQuitMessage(4);
	}

	::EndPaint(handle, &paintStruct);
}

void CCanvasWindow::setMoveAccelerations(LPARAM lParam)
{
	TRACKMOUSEEVENT trackmouse;
	trackmouse.cbSize = sizeof(trackmouse);
	trackmouse.dwFlags = TME_LEAVE;
	trackmouse.hwndTrack = handle;
	trackmouse.dwHoverTime = HOVER_DEFAULT;
	::TrackMouseEvent(&trackmouse);


	int x = LOWORD(lParam);
	int y = HIWORD(lParam);
	model->GetSpaceship()->SetRotateAcceleration((x - centerX) / (centerX + 0.01));
	model->GetSpaceship()->SetMoveAcceleration((centerY - y) / (centerY + 0.01));
}

void CCanvasWindow::onRightButton(LPARAM lParam)
{
	TRACKMOUSEEVENT trackmouse;
	trackmouse.cbSize = sizeof(trackmouse);
	trackmouse.dwFlags = TME_LEAVE;
	trackmouse.hwndTrack = handle;
	trackmouse.dwHoverTime = HOVER_DEFAULT;
	::TrackMouseEvent(&trackmouse);


	int x = LOWORD(lParam);
	int y = HIWORD(lParam);
	model->LaunchMissle((x - centerX) / (centerX + 0.01), (centerY - y) / (centerY + 0.01));
}


void CCanvasWindow::stopMoving(LPARAM lParam)
{
	if (leftPressed) {
		model->GetSpaceship()->SetMoveAcceleration(0);
		model->GetSpaceship()->SetRotateAcceleration(0);
	}
	leftPressed = false;
}

void CCanvasWindow::drawEraseRectangle(HDC paintDC, const int width, const int height)
{
	RECT clientContext;
	clientContext.bottom = height;
	clientContext.right = width;
	clientContext.left = 0;
	clientContext.top = 0;
	FillRect(paintDC, &clientContext, backgroundBrush);
	FillRect(paintDC, &clientContext, backgroundBrush);
}

void CCanvasWindow::drawTranformToWorld(HDC paintDC)
{
	XFORM xform;
	double alpha = model->GetSpaceship()->GetAngle();
	double x = model->GetSpaceship()->GetLocation().GetX();
	double y = model->GetSpaceship()->GetLocation().GetY();
	xform.eM11 = cos(alpha);
	xform.eM12 = sin(alpha);
	xform.eM21 = sin(alpha);
	xform.eM22 = -cos(alpha);
	xform.eDx = centerX - x*xform.eM11 - y*xform.eM21;
	xform.eDy = centerY - x*xform.eM12 - y*xform.eM22;
	if (SetWorldTransform(paintDC, &xform) == 0) {
		MessageBox(handle, L"Error. Can't set world transform", L"Error", MB_ICONERROR | MB_OK);
	}
}

void CCanvasWindow::drawTranformBackToScreen(HDC paintDC)
{
	XFORM xform;
	xform.eDx = 0;
	xform.eDy = 0;
	xform.eM11 = 1;
	xform.eM12 = 0;
	xform.eM21 = 0;
	xform.eM22 = 1;
	if (SetWorldTransform(paintDC, &xform) == 0) {
		MessageBox(handle, L"Error. Can't set default transform", L"Error", MB_ICONERROR | MB_OK);
	}
}

void CCanvasWindow::drawPaintObjects(HDC paintDC)
{
	CStar::InitStarsPaint(paintDC);
	for (size_t i = 0; i < model->GetStars().size(); i++) {
		model->GetStars()[i].Draw(paintDC);
	}
	for (size_t i = 0; i < model->GetObjects().size(); i++) {
		model->GetObjects()[i]->Draw(paintDC);
	}
}

void CCanvasWindow::drawRocket(HDC paint)
{
	auto spaceship = model->GetSpaceship();
	if (spaceship->GetFuel() > 0) {
		double acceleration = spaceship->GetMoveAcceleration();
		if (acceleration > 0) {
			::MoveToEx(paint, centerX, centerY + RocketImageSize / 2, 0);
			::SelectObject(paint, fireBackPen);
			::LineTo(paint, centerX, centerY + RocketImageSize / 2 + FireLength * acceleration);
		}
		if (acceleration < 0) {
			::MoveToEx(paint, centerX, centerY - RocketImageSize / 2, 0);
			::SelectObject(paint, fireFrontPen);
			::LineTo(paint, centerX, centerY - RocketImageSize / 2 + FireLength * acceleration);
		}
		acceleration = spaceship->GetRotateAcceleration();
		::SelectObject(paint, fireSidePen);
		if (acceleration > 0) {
			int x = centerX - RocketImageSize / 3.8;
			int y = centerY + RocketImageSize / 2.2;
			::MoveToEx(paint, x, y, 0);
			::LineTo(paint, x - FireLength * acceleration / 2, y);
			x = centerX;
			y = centerY - RocketImageSize / 2;
			::MoveToEx(paint, x, y, 0);
			::LineTo(paint, x + FireLength * acceleration / 2, y);
		}
		if (acceleration < 0) {
			int x = centerX + RocketImageSize / 3.8;
			int y = centerY + RocketImageSize / 2.2;
			::MoveToEx(paint, x, y, 0);
			::LineTo(paint, x - FireLength * acceleration / 2, y);
			x = centerX;
			y = centerY - RocketImageSize / 2;
			::MoveToEx(paint, x, y, 0);
			::LineTo(paint, x + FireLength * acceleration / 2, y);
		}
	}
	::DrawIconEx(paint, centerX - RocketImageSize / 2, centerY - RocketImageSize / 2, rocketImage, RocketImageSize, RocketImageSize, 0, 0, DI_NORMAL);
}

void CCanvasWindow::drawContent(HDC paintDC, const int width, const int height)
{
	drawEraseRectangle(paintDC, width, height);
	drawTranformToWorld(paintDC);
	drawPaintObjects(paintDC);
	drawTranformBackToScreen(paintDC);
	drawRocket(paintDC);
}

void CCanvasWindow::drawTimerTick()
{
	DWORD currentTime = GetTickCount();
	model->Iterate((currentTime - lastTime) / 1000.0);
	lastTime = currentTime;

	COverlappedWindow::GetWindowByHandle(::GetParent(handle))->UpdateModelInfo();

	RECT rect;
	::GetClientRect(handle, &rect);
	InvalidateRect(handle, &rect, true);
}

void CCanvasWindow::resize(const RECT* area)
{
	destroyDoubleBuffer();
	HDC tmp = GetDC(handle);
	bitmapContext = CreateCompatibleDC(tmp);
	bitmapWidth = area->right - area->left + 1;
	bitmapHeight = area->bottom - area->top + 1;
	centerX = bitmapWidth / 2;
	centerY = bitmapHeight / 2;
	bitmap = CreateCompatibleBitmap(tmp, bitmapWidth, bitmapHeight);
	SelectObject(bitmapContext, bitmap);
	if (SetGraphicsMode(bitmapContext, GM_ADVANCED) == 0) {
		MessageBox(handle, L"Error. Can't set graphycs mode", L"Error", MB_ICONERROR | MB_OK);
	}
	ReleaseDC(handle, tmp);
}


void CCanvasWindow::destroyDoubleBuffer()
{
	if ((bitmapContext != 0) || (bitmap != 0)) {
		DeleteDC(bitmapContext);
		DeleteObject(bitmap);
	}
}

void CCanvasWindow::createResouces()
{
	startTime = GetTickCount();
	lastTime = startTime;

	COLORREF fire = RGB(240, 160, 20);
	fireBackPen = CreatePen(PS_SOLID, FireWidth, fire);
	fireFrontPen = CreatePen(PS_SOLID, (int)(FireWidth / CSpaceship::FrontEngineDivider), fire);
	fireSidePen = CreatePen(PS_SOLID, (int)(FireWidth / CSpaceship::SideEngineDivider), fire);

	backgroundBrush = CreateSolidBrush(RGB(24, 24, 24));
	CStar::CreateBrush();
	HINSTANCE instance = ::GetModuleHandle(0);
	rocketImage = static_cast<HICON>(::LoadImage(instance, MAKEINTRESOURCE(IDI_ROCKET), IMAGE_ICON, RocketImageSize, RocketImageSize, LR_DEFAULTCOLOR));
	if (rocketImage == 0) {
		::MessageBox(handle, L"Can't open rocket image", L"Error", MB_ICONERROR | MB_OK);
	}

	CTarget::CreateBrushes();
	CFire::CreateBrushes();
	CMissle::CreateBrushes();
}

void CCanvasWindow::clearResources()
{
	DeleteObject(fireBackPen);
	DeleteObject(fireFrontPen);
	DeleteObject(fireSidePen);

	DeleteObject(backgroundBrush);
	CStar::FreeBrush();
	CTarget::ClearBrushes();
	CFire::ClearBrushes();
	CMissle::ClearBrushes();
	DestroyIcon(rocketImage);
}