#pragma once
#include "Model.h"

class CCanvasWindow {
public:
    CCanvasWindow(CModel* model);
    CCanvasWindow();
    ~CCanvasWindow();

	static bool RegisterClass();

	bool Create(HWND parent);

	void Show(int cmdShow);

	HWND GetHandle();

    void SetModel(CModel * model);

protected:
	void OnDestroy();

private:
	HWND handle;
	DWORD startTime;
    DWORD lastTime;
	UINT_PTR timer;
	HBITMAP bitmap;
    HDC bitmapContext;
    HICON rocketImage;

    HPEN fireBackPen;
    HPEN fireFrontPen;
    HPEN fireSidePen;
    HBRUSH backgroundBrush;

	int bitmapWidth;
	int bitmapHeight;
    int centerX;
    int centerY;
    bool leftPressed;
    bool rightPressed;

    CModel* model;

	static const wchar_t* ClassName;
    static const int DrawTimerID;
    static const int RocketImageSize;
    static const int FireWidth;
    static const int FireLength;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

	void onPaint();

    void setMoveAccelerations(LPARAM lParam);

	void onRightButton(LPARAM lParam);

    void stopMoving(LPARAM lParam);

    void drawEraseRectangle(HDC paintDC, const int width, const int height);

    void drawTranformToWorld(HDC paintDC);

    void drawTranformBackToScreen(HDC paintDC);

    void drawPaintObjects(HDC paintDC);

    void drawRocket(HDC paintDC);

	void drawContent(HDC paintDC, const int width, const int height);

	void drawTimerTick();

	void resize(const RECT* area);

    void destroyDoubleBuffer();

    void createResouces();

    void clearResources();
};


