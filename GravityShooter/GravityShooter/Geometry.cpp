﻿#include "stdafx.h"
#include "Geometry.h"


CVector::CVector() : x(0), y(0)
{
}


CVector::~CVector()
{
}

CVector::CVector(double x, double y) : x(x), y(y)
{

}


double CVector::GetX() const
{
    return x;
}

void CVector::SetY(double y)
{
    CVector::y = y;
}

CVector CVector::operator+(const CVector & other) const
{
    CVector result(*this);
    result += other;
    return result;
}

CVector CVector::operator-(const CVector & other) const
{
    CVector result(*this);
    result -= other;
    return result;
}

CVector CVector::operator*(const double k) const
{
    CVector result(*this);
    result *= k;
    return result;
}

CVector CVector::operator/(const double k) const
{
    CVector result(*this);
    result /= k;
    return result;
}

CVector CVector::operator-() const
{
    return (*this)*(-1);
}

CVector& CVector::operator+=(const CVector & other)
{
    x += other.x;
    y += other.y;
    return (*this);
}

CVector& CVector::operator-=(const CVector & other)
{
    x -= other.x;
    y -= other.y;
    return (*this);
}

CVector& CVector::operator*=(const double k)
{
    x *= k;
    y *= k;
    return (*this);
}

CVector & CVector::operator/=(const double k)
{
    x /= k;
    y /= k;
    return(*this);
}

double CVector::Length() const
{
    return sqrt(x*x + y*y);
}


double CVector::GetY() const
{
    return y;
}

void CVector::SetX(double x)
{
    CVector::x = x;
}

CPointD::CPointD() : x(0), y(0)
{
}

CPointD::CPointD(double x, double y) : x(x), y(y)
{

}

CPointD::~CPointD()
{
}

double CPointD::GetX() const
{
    return x;
}

void CPointD::SetX(double x)
{
    this->x = x;
}

double CPointD::GetY() const
{
    return y;
}

void CPointD::SetY(double y)
{
    this->y = y;
}

CPointD & CPointD::operator+=(const CVector & vector)
{
    x += vector.GetX();
    y += vector.GetY();
    return *this;
}

CPointD CPointD::operator+(const CVector & vector) const
{
    CPointD result(*this);
    result += vector;
    return result;
}

CVector CPointD::operator-(const CPointD & other) const
{
    return CVector(other.x - x, other.y - y);
}


