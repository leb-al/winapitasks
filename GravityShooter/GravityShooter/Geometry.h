#pragma once

class CVector {
public:
    CVector();

    CVector(double x, double y);

    ~CVector();

    double GetX() const;

    void SetX(double x);

    double GetY() const;

    void SetY(double y);

    CVector operator + (const CVector& other) const;

    CVector operator - (const CVector& other) const;

    CVector operator * (const double k) const;

    CVector operator / (const double k) const;

    CVector operator - () const;

    CVector& operator += (const CVector& other);

    CVector& operator -= (const CVector& other);

    CVector& operator *= (const double k);

    CVector& operator /= (const double k);

    double Length() const;

private:
    double x, y;
};

class CPointD {
public:
    CPointD();

    CPointD(double x, double y);

    ~CPointD();

    double GetX() const;

    void SetX(double x);

    double GetY() const;

    void SetY(double y);

    CPointD& operator += (const CVector& vector);

    CPointD operator + (const CVector& vector) const;

    CVector operator - (const CPointD& other) const;


private:
    double x, y;
};
