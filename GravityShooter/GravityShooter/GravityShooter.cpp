#include "stdafx.h"
#include "GravityShooter.h"
#include "OverlappedWindow.h"
#include "CanvasWindow.h"

int WINAPI wWinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPWSTR    lpCmdLine,
    int       nCmdShow)
{
    if (!COverlappedWindow::RegisterClass()) {
        return 1;
    }
    if (!CCanvasWindow::RegisterClass()) {
        return 2;
    }
    COverlappedWindow window;
    if (!window.Create()) {
        return 3;
    }
    window.Show(nCmdShow);


    HACCEL acceleartors = ::LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GRAVITYSHOOTER));
    MSG message;
    BOOL getMessageResult = 0;
    while ((getMessageResult = ::GetMessage(&message, 0, 0, 0)) != 0) {
        if (getMessageResult == -1) {
            return 4;
        }
        if (!::TranslateAccelerator(window.GetHandle(), acceleartors, &message) && !::IsDialogMessage(window.GetDialog(), &message)) {
            ::TranslateMessage(&message);
            ::DispatchMessage(&message);
        }
    }
    DestroyAcceleratorTable(acceleartors);

    return 0;
}



