#include "stdafx.h"
#include "Model.h"


CModel::CModel() : stars(), objects()
{
	init();
}


CModel::~CModel()
{
}

void CModel::Iterate(double dt)
{
	if (!active) {
		return;
	}
	for (int i = 0; i < objects.size(); ++i) {
		for (int j = 0; j < i; ++j) {
			CVector force = (objects[j]->GetLocation()) - (objects[i]->GetLocation());
			force *= g * objects[i]->GetM() * objects[j]->GetM() / pow(force.Length(), 3);
			objects[i]->Accelerate(-force, dt);
			objects[j]->Accelerate(force, dt);
		}
	}
	for (int i = 0; i < objects.size(); ++i) {
		for (int j = 0; j < i; ++j) {
			if (objects[i]->Contacted(objects[j])) {
				if ((objects[i]->GetType() == CPhysicalObject::SpaceshipType) || (objects[j]->GetType() == CPhysicalObject::SpaceshipType)) {
					active = false;
					victory = false;
					return;
				}

				bool imissle = (objects[i]->GetType() == CPhysicalObject::MissleType);
				if (imissle || (objects[j]->GetType() == CPhysicalObject::MissleType)) {
					int missleInd = (imissle ? i : j);
					int otherInd = (imissle ? j : i);
					auto missle = objects[missleInd];
					CFire* fire = new CFire();
					fire->SetLocation(missle->GetLocation());
					fire->SetSpeed(objects[otherInd]->GetSpeed());
					objects[missleInd] = std::shared_ptr<CPhysicalObject>(fire);
				}

				bool ifire = (objects[i]->GetType() == CPhysicalObject::FireType);
				if (ifire || (objects[j]->GetType() == CPhysicalObject::FireType)) {
					int otherInd = (imissle ? j : i);
					auto other = objects[otherInd];
					if (other->GetType() == CPhysicalObject::TargetType) {
						CFire* fire = new CFire();
						fire->SetLocation(other->GetLocation());
						fire->SetSpeed(objects[otherInd]->GetSpeed());
						objects[otherInd] = std::shared_ptr<CPhysicalObject>(fire);
						--targets;
					}
				}
			}
		}
	}
	for (size_t i = 0; i < objects.size(); i++) {
		if (objects[i]->GetType() == CPhysicalObject::FireType) {
			CFire* fire = dynamic_cast<CFire*>(objects[i].get());
			if (fire->GetT() > 4) {
				objects.erase(objects.begin() + i);
				--i;
			}
		}
	}
	for (size_t i = 0; i < objects.size(); i++) {
		objects[i]->Move(dt);
	}
	if (targets == 0) {
		victory = true;
		active = false;
	}
}

bool CModel::Active()
{
	return active;
}

std::shared_ptr<CSpaceship>& CModel::GetSpaceship()
{
	return spaceship;
}

std::vector<CStar>& CModel::GetStars()
{
	return stars;
}

const std::vector<ObjectPtr>& CModel::GetObjects() const
{
	return objects;
}

int CModel::GetTargets()
{
	return targets;
}

void CModel::LoadModel(const wchar_t* config)
{
	objects.clear();
	stars.clear();
	init();
	std::wstringstream input(config);
	double g;
	input >> g;
	this->g = g;
	loadObject(input, spaceship.get());
	bool haveobjects = true;
	int type;
	while (haveobjects && (input)) {
		CPhysicalObject* object = 0;
		input >> type;
		switch (type) {
			case 0:
				haveobjects = 0;
				break;
			case 1:
				int r, g, b;
				input >> r >> g >> b;
				object = new CPlanet(r, g, b);
				break;
			case 2:
				++targets;
				object = new CTarget();
				break;
			default:
				break;
		}
		if (object != 0) {
			loadObject(input, object);
			objects.push_back(std::shared_ptr<CPhysicalObject>(object));
		}
	}
}

void CModel::LaunchMissle(double x, double y)
{
	if ((y < 0) || (spaceship->GetMissleCount() == 0)) {
		return;
	}
	CMissle* missle = new CMissle();
	double r = sqrt(x*x + y*y);
	if (r > 1) {
		x /= r;
		y /= r;
	}
	double alpha = spaceship->GetAngle();
	double speedX = (x * cos(alpha) - y * sin(alpha)) * missleSpeed;
	double speedY = (x * sin(alpha) + y * cos(alpha)) * missleSpeed;
	auto speed = CVector(speedX, speedY) + spaceship->GetSpeed();
	missle->SetSpeed(speed);
	spaceship->LaunchMissle(speed);
	x = spaceship->GetLocation().GetX();
	y = spaceship->GetLocation().GetY();
	missle->SetLocation(CPointD(x - CSpaceship::SpaceshipHalfLength * sin(alpha), y + CSpaceship::SpaceshipHalfLength*cos(alpha)));
	objects.push_back(std::shared_ptr<CPhysicalObject>(missle));
}

int CModel::Victory()
{
	return victory;
}

const int CModel::missleSpeed = 50;

void CModel::loadObject(std::wstringstream & input, CPhysicalObject* object)
{
	double x, y;
	input >> x >> y;
	object->SetLocation(CPointD(x, y));
	input >> x >> y;
	object->SetSpeed(CVector(x, y));
	double w, alpha;
	input >> w >> alpha;
	object->SetAngle(alpha);
	object->SetW(w);
	double m, r;
	input >> m >> r;
	object->SetM(m);
	object->SetR(r);
}

void CModel::init()
{
	spaceship = std::shared_ptr<CSpaceship>(new CSpaceship());
	objects.push_back(spaceship);
	srand(::GetTickCount());
	targets = 0;
	victory = false;
	active = true;
	for (size_t i = 0; i < 500; i++) {
		int x = -500 + rand() % 3000;
		int y = -500 + rand() % 3000;
		double r = (rand() % 4000) / 1000.0 + 0.5;
		CStar star;
		star.SetR(r);
		star.SetLocation(CPointD(x, y));
		stars.push_back(star);
	}
}