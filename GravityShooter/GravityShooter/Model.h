#pragma once

#include "PhysicalObject.h"
#include "SpaceObjects.h"

class CModel {
public:
    CModel();
    ~CModel();

    void Iterate(double t);
    bool Active();
    std::shared_ptr<CSpaceship>& GetSpaceship();
    std::vector<CStar>& GetStars();
    const std::vector<ObjectPtr>& GetObjects() const;
    int GetTargets();
    void LoadModel(const wchar_t * config);
	void LaunchMissle(double x, double y);
	int Victory();

private:
    std::vector<ObjectPtr> objects;
    std::shared_ptr<CSpaceship> spaceship;
    std::vector<CStar> stars;
    double g;
    bool active;
	int targets;
	bool victory;

	static const int missleSpeed;

    void loadObject(std::wstringstream& input, CPhysicalObject* object);
	
	void init();
};

