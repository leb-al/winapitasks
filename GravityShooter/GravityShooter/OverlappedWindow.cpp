﻿#include "stdafx.h"
#include "OverlappedWindow.h"
#include "Resource.h"
#include <commdlg.h>
#include <sstream>

#define MAX_RESOURCE_LENGTH 100
#define MAX_LEVEL_LENGTH 4096

COverlappedWindow::COverlappedWindow() : COverlappedWindow(new CModel())
{
}

COverlappedWindow::COverlappedWindow(CModel* modelValue) : model(modelValue), dialog(0), canvasWindow(0)
{
	lastLevel = new wchar_t[MAX_LEVEL_LENGTH];
}

COverlappedWindow::~COverlappedWindow()
{
	delete[] lastLevel;
}

bool COverlappedWindow::RegisterClass()
{
	HMODULE instance = GetModuleHandleW(nullptr);

	WNDCLASSEX windowClassInforamtion;
	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);
	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 2 * sizeof(LONG_PTR);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_GRAVITYSHOOTER));
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = backBrush;
	windowClassInforamtion.lpszMenuName = MAKEINTRESOURCE(IDM_GRAVITYSHOOTER);
	windowClassInforamtion.lpszClassName = COverlappedWindow::ClassName;
	windowClassInforamtion.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_SMALL));

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}


bool COverlappedWindow::Create()
{
	canvasWindow.SetModel(model);
	HINSTANCE instance = GetModuleHandleW(nullptr);
	wchar_t title[MAX_RESOURCE_LENGTH];
	::LoadString(instance, IDS_APP_TITLE, title, MAX_RESOURCE_LENGTH);
	handle = CreateWindowEx(0, COverlappedWindow::ClassName, title, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 1000, 600, 0, 0, instance, this);
	if (handle == 0)
		return false;

	labelFuel = ::CreateWindowEx(0, L"STATIC", 0, WS_CHILD | WS_VISIBLE | ES_CENTER, 0, 0, ControlWidth, PanelHeight, this->handle, 0, instance, 0);
	wchar_t text[MAX_RESOURCE_LENGTH];
	::LoadString(instance, IDS_FUEL, text, MAX_RESOURCE_LENGTH);
	::SetWindowText(labelFuel, text);
	labelTargets = ::CreateWindowEx(0, L"STATIC", 0, WS_CHILD | WS_VISIBLE, 0, 0, ControlWidth, PanelHeight, this->handle, 0, instance, 0);
	::LoadString(instance, IDS_TARGETS, text, MAX_RESOURCE_LENGTH);
	::SetWindowText(labelTargets, text);
	targetsPrefix = text;
	labelMissles = ::CreateWindowEx(0, L"STATIC", 0, WS_CHILD | WS_VISIBLE, 0, 0, ControlWidth, PanelHeight, this->handle, 0, instance, 0);
	::LoadString(instance, IDS_MISSLES, text, MAX_RESOURCE_LENGTH);
	::SetWindowText(labelMissles, text);
	misslesPrefix = text;
	progressFuel = ::CreateWindowEx(0, PROGRESS_CLASS, 0, WS_CHILD | WS_VISIBLE, 0, 0, ControlWidth, PanelHeight, this->handle, 0, instance, 0);
	::SendMessage(progressFuel, PBM_SETRANGE32, 0, (int)(CSpaceship::MaxFuel * FuelCoef));
	::SendMessage(progressFuel, PBS_SMOOTH, 1, 0);

	return canvasWindow.Create(handle);
}

void COverlappedWindow::Show(int cmdShow)
{
	cmdShow = SW_MAXIMIZE;
	loadLevel(IDS_FIELD01);
	::ShowWindow(handle, cmdShow);
	canvasWindow.Show(SW_SHOW);
	::SetFocus(canvasWindow.GetHandle());
}

HWND COverlappedWindow::GetHandle()
{
	return handle;
}

COverlappedWindow * COverlappedWindow::GetWindowByHandle(HWND handle)
{
	return reinterpret_cast<COverlappedWindow*>(GetWindowLongPtr(handle, 0));
}

HWND COverlappedWindow::GetDialog()
{
	return dialog;
}

void COverlappedWindow::UpdateModelInfo()
{
	::SetWindowText(labelTargets, (targetsPrefix + std::to_wstring(model->GetTargets())).c_str());
	::SetWindowText(labelMissles, (misslesPrefix + std::to_wstring(model->GetSpaceship()->GetMissleCount())).c_str());
	::SendMessage(progressFuel, PBM_SETPOS, model->GetSpaceship()->GetFuel() * FuelCoef, 0);
	if (active && (!model->Active())) {
		active = false;
		wchar_t title[MAX_RESOURCE_LENGTH];
		wchar_t message[MAX_RESOURCE_LENGTH];
		HINSTANCE instance = ::GetModuleHandle(0);
		::LoadString(instance, IDS_FINSH, title, MAX_RESOURCE_LENGTH);
		if (model->Victory()) {
			::LoadString(instance, IDS_VICTORY, message, MAX_RESOURCE_LENGTH);
		}
		else {
			::LoadString(instance, IDS_LOSE, message, MAX_RESOURCE_LENGTH);
		}
		::MessageBox(handle, message, title, MB_ICONINFORMATION | MB_OK);
	}
}

void COverlappedWindow::OnDestroy()
{
	::DeleteObject(backBrush);
	delete model;
	PostQuitMessage(0);
}

COLORREF COverlappedWindow::backColor = RGB(5, 40, 20);
HBRUSH COverlappedWindow::backBrush = CreateSolidBrush(COverlappedWindow::backColor);

const wchar_t* COverlappedWindow::ClassName = L"COverlappedWindow";
const int COverlappedWindow::PanelHeight = 30;
const int COverlappedWindow::ControlWidth = 100;
const int COverlappedWindow::FuelCoef = 10;

LRESULT __stdcall COverlappedWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLongPtr(handle, 0, reinterpret_cast<LONG_PTR>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	COverlappedWindow* wndPtr = GetWindowByHandle(handle);
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_SIZING:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->onResize();
			return result;
		}
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->onResize();
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->onResize();
			return result;
		}
		case WM_CLOSE:
			if (wndPtr->finishDialog(::GetModuleHandle(0), true)) {
				return DefWindowProc(handle, message, wParam, lParam);
			}
			else {
				return 0;
			}
		case WM_COMMAND:
			wndPtr->onCommandRecieved(wParam, lParam);
			break;
		case WM_CTLCOLOREDIT:
			return wndPtr->onCtlColorEdit(handle, wParam, lParam);
		case WM_GETMINMAXINFO:
			return wndPtr->onGetMinMaxInfo(lParam);
		case WM_CTLCOLORSTATIC:
			return wndPtr->onCtrColorStatic(wParam, lParam);
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void COverlappedWindow::onResize()
{
	RECT area;
	::GetClientRect(handle, &area);
	if (canvasWindow.GetHandle() != 0) {
		SetWindowPos(canvasWindow.GetHandle(), HWND_TOP, area.left, area.top, area.right, area.bottom - PanelHeight, 0);
		SetWindowPos(labelFuel, HWND_TOP, area.left, area.bottom - PanelHeight, ControlWidth, PanelHeight, 0);
		SetWindowPos(labelTargets, HWND_TOP, area.left + ControlWidth * 4, area.bottom - PanelHeight, ControlWidth, PanelHeight, 0);
		SetWindowPos(labelMissles, HWND_TOP, area.left + ControlWidth * 5, area.bottom - PanelHeight, ControlWidth, PanelHeight, 0);
		SetWindowPos(progressFuel, HWND_TOP, area.left + ControlWidth, area.bottom - PanelHeight, ControlWidth * 3, PanelHeight, 0);
	}
}

bool COverlappedWindow::finishDialog(HINSTANCE instance, bool exit)
{
	if (model->Active()) {
		wchar_t message[MAX_RESOURCE_LENGTH];
		wchar_t header[MAX_RESOURCE_LENGTH];
		if (exit) {
			::LoadString(instance, IDS_EXITMBOXTITLE, header, MAX_RESOURCE_LENGTH);
			::LoadString(instance, IDS_EXITMBOX, message, MAX_RESOURCE_LENGTH);
		}
		else {
			::LoadString(instance, IDS_NEWGAMEMBOXTITLE, header, MAX_RESOURCE_LENGTH);
			::LoadString(instance, IDS_NEWGAMEMBOX, message, MAX_RESOURCE_LENGTH);

		}
		int result = ::MessageBoxW(this->handle, message, header, MB_YESNO | MB_ICONQUESTION);
		switch (result) {
			case IDYES:
				return true;
			case IDNO:
				return false;
			default:
				MessageBox(handle, L"Something strange from mbox", L"Debug error", MB_ICONERROR | MB_OK);
				return false;
		}
	}
	else {
		return true;
	}
}

INT_PTR COverlappedWindow::onCtrColorStatic(WPARAM wParam, LPARAM lParam)
{
	HDC context = reinterpret_cast<HDC>(wParam);
	::SetBkColor(context, backColor);
	::SetTextColor(context, RGB(255, 0, 0));
	return reinterpret_cast<INT_PTR>(backBrush);
}

INT_PTR CALLBACK AboutBoxWindowProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}


void COverlappedWindow::onCommandRecieved(WPARAM wParam, LPARAM lParam)
{
	HINSTANCE instance = GetModuleHandleW(nullptr);
	switch (LOWORD(wParam)) {
		case IDM_EXIT:
			::SendMessage(handle, WM_CLOSE, 0, 0);
			break;
		case IDM_NEWGAME:
			if (finishDialog(instance, false)) {
				loadLevel();
			}
			break;
		case IDM_ABOUT:
			::DialogBox(instance, MAKEINTRESOURCE(IDD_ABOUTBOX), handle, AboutBoxWindowProc);
			break;
		case IDM_FIELD01:
			if (finishDialog(instance, false)) {
				loadLevel(IDS_FIELD01);
			}
			break;
		case IDM_FIELD02:
			if (finishDialog(instance, false)) {
				loadLevel(IDS_FIELD02);
			}
			break;
		case IDM_FIELD03:
			if (finishDialog(instance, false)) {
				loadLevel(IDS_FIELD03);
			}
			break;
		default:
			break;
	}
}

LRESULT COverlappedWindow::onCtlColorEdit(HWND handle, WPARAM wParam, LPARAM lParam)
{
	HDC context = reinterpret_cast<HDC>(wParam);
	::SetBkColor(context, backColor);
	::SetTextColor(context, RGB(255, 0, 0));
	return reinterpret_cast<LRESULT>(backBrush);
}

int COverlappedWindow::onGetMinMaxInfo(LPARAM lParam)
{
	MINMAXINFO *pInfo = (MINMAXINFO *)lParam;
	POINT Min = { 700, 500 };
	POINT  Max = { 2000, 2000 };
	pInfo->ptMinTrackSize = Min;
	pInfo->ptMaxTrackSize = Max;
	return 0;
}

void COverlappedWindow::loadLevel(int id)
{
	::LoadString(::GetModuleHandle(0), id, lastLevel, MAX_LEVEL_LENGTH);
	loadLevel();
}

void COverlappedWindow::loadLevel()
{
	active = true;
	model->LoadModel(lastLevel);
}