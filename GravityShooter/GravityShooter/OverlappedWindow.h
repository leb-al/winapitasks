#pragma once

#include "Model.h"
#include "CanvasWindow.h"

class COverlappedWindow {
public:
	COverlappedWindow();
    COverlappedWindow(CModel * model);
    ~COverlappedWindow();

	static bool RegisterClass();

	bool Create();
	void Show(int cmdShow);

	HWND GetHandle();

	static COverlappedWindow* GetWindowByHandle(HWND handle);

	HWND GetDialog();

    void UpdateModelInfo();

protected:
	void OnDestroy();

private:
	HWND handle;
	HWND dialog;
    HWND labelFuel;
    HWND progressFuel;
	HWND labelTargets;
	HWND labelMissles;

    CCanvasWindow canvasWindow;

	std::wstring targetsPrefix;
	std::wstring misslesPrefix;
    wchar_t* lastLevel;
	bool active;

    CModel* model;
	
    static COLORREF backColor;
    static HBRUSH backBrush;

	static const wchar_t* ClassName;
    static const int PanelHeight;
    static const int ControlWidth;
    static const int FuelCoef;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

    void onResize();

    bool finishDialog(HINSTANCE instance, bool exit);

    INT_PTR onCtrColorStatic(WPARAM wParam, LPARAM lParam);

	void onCommandRecieved(WPARAM wParam, LPARAM lParam);

	LRESULT onCtlColorEdit(HWND handle, WPARAM wParam, LPARAM lParam);

    int onGetMinMaxInfo(LPARAM lParam);

    void loadLevel(int id);

    void loadLevel();
};


