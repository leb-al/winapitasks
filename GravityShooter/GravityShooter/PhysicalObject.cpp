#include "stdafx.h"
#include "SpaceObjects.h"


CPhysicalObject::CPhysicalObject()
{
    m = 0;
    r = 0;
    w = 0;
    angle = 0;
    speed = CVector();
    location = CPointD();
}


CPhysicalObject::~CPhysicalObject()
{
}

void CPhysicalObject::Move(double t)
{
    location += speed * t;
    angle += w * t;
}

double CPhysicalObject::GetM() const
{
    return m;
}

void CPhysicalObject::SetM(double m)
{
	CPhysicalObject::m = m;
}

double CPhysicalObject::GetR() const
{
    return r;
}

void CPhysicalObject::SetR(double r)
{
    this->r = r;
}

const CPointD &CPhysicalObject::GetLocation() const
{
    return location;
}

void CPhysicalObject::SetSpeed(const CVector &speed)
{
    CPhysicalObject::speed = speed;
}

double CPhysicalObject::GetAngle() const
{
    return angle;
}

void CPhysicalObject::SetAngle(double angle)
{
    CPhysicalObject::angle = angle;
}

double CPhysicalObject::GetW() const
{
    return w;
}

void CPhysicalObject::SetW(double w)
{
    CPhysicalObject::w = w;
}

void CPhysicalObject::Accelerate(const CVector & force, double dt)
{
    speed += force*dt / m;
}

bool CPhysicalObject::Contacted(const std::shared_ptr<CPhysicalObject>& other) const
{
    CVector distance = other->GetLocation() - location;
    return (distance.Length() <= (other->GetR() + GetR()));
}

int CPhysicalObject::GetType() const
{
	return DefaultType;
}


const int CPhysicalObject::DefaultType = 0;
const int CPhysicalObject::StarType = 3;
const int CPhysicalObject::PlanetType = 1;
const int CPhysicalObject::TargetType = 2;
const int CPhysicalObject::MissleType = 4;
const int CPhysicalObject::FireType = 5;
const int CPhysicalObject::SpaceshipType = 6;

const CVector &CPhysicalObject::GetSpeed() const
{
    return speed;
}

void CPhysicalObject::SetLocation(const CPointD &location)
{
    CPhysicalObject::location = location;
}

void CPhysicalObject::Draw(HDC canvas) const
{
}
