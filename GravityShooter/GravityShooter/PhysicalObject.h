#pragma once

#include "Geometry.h"

class CPhysicalObject {
public:
    CPhysicalObject();
    virtual ~CPhysicalObject();

    virtual void Move(double t);

    double GetM() const;

	virtual void SetM(double m);

    virtual double GetR() const;

    void SetR(double r);

    const CPointD & GetLocation() const;

    void SetLocation(const CPointD &location);

    virtual void Draw(HDC canvas) const;

    const CVector & GetSpeed() const;

    void SetSpeed(const CVector &speed);

    double GetAngle() const;

    void SetAngle(double angle);

    double GetW() const;

    void SetW(double w);

    void Accelerate(const CVector& force, double dt);

    bool Contacted(const std::shared_ptr<CPhysicalObject>& other) const;

	virtual int GetType() const;

	static const int DefaultType;
	static const int StarType;
	static const int PlanetType;
	static const int TargetType;
	static const int MissleType;
	static const int FireType;
	static const int SpaceshipType;

protected:
    double m;
    double r;
    CVector speed;
    CPointD location;
    double angle;
    double w;
};

typedef std::shared_ptr<CPhysicalObject> ObjectPtr;

