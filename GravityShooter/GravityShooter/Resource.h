//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется GravityShooter.rc
//
#define IDC_MYICON                      2
#define IDD_GRAVITYSHOOTER_DIALOG       102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDS_EXITMBOX                    104
#define IDM_EXIT                        105
#define IDS_EXITMBOXTITLE               105
#define IDS_FUEL                        106
#define IDI_GRAVITYSHOOTER              107
#define IDS_TARGETS                     107
#define IDI_SMALL                       108
#define IDS_FIELD01                     108
#define IDC_GRAVITYSHOOTER              109
#define IDM_GRAVITYSHOOTER              109
#define IDS_NEWGAMEMBOX                 110
#define IDS_NEWGAMETITLE                111
#define IDS_NEWGAMEMBOXTITLE            111
#define IDS_FIELD02                     112
#define IDS_MISSLES                     113
#define IDS_VICTORY                     114
#define IDS_LOSE                        115
#define IDS_FINSH                       116
#define IDS_FIELD03                     117
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       134
#define IDI_ROCKET                      134
#define ID_32771                        32771
#define IDM_NEWGAME                     32772
#define ID_32773                        32773
#define IDM_FIELD01                     32774
#define ID_32777                        32777
#define IDM_FIELD02                     32778
#define ID_32779                        32779
#define IDM_FIELD03                     32780
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
