﻿#include "stdafx.h"
#include "SpaceObjects.h"


CSpaceship::CSpaceship()
{
	fuel = MaxFuel;
	misslesCount = MisslesStart;
	moveAcceleration = 0;
	rotateAcceleration = 0;
	updateMass();
}


CSpaceship::~CSpaceship()
{
}

void CSpaceship::SetM(double m)
{
	updateMass();
}

void CSpaceship::Draw(HDC paint) const
{

}

void CSpaceship::Move(double dt)
{
	CPhysicalObject::Move(dt);
	if (fuel > 0) {
		double dm = ConsumptionSpeed*abs(moveAcceleration)*dt;
		if (moveAcceleration < 0) {
			dm /= FrontEngineDivider;
		}
		fuel -= dm;
		if (fuel < 0) {
			dm += fuel;
			fuel = 0;
			moveAcceleration *= (dm + fuel) / dm;
		}
		CVector acceleration(-sin(angle), cos(angle));
		acceleration *= RelativeSpeed * dm / m; // уравнение Мещерского 
		if (moveAcceleration < 0) {
			acceleration *= (-1);
		}
		speed += acceleration;
		if (fuel >= 0) {
			dm = ConsumptionSpeed * abs(rotateAcceleration) * dt / SideEngineDivider;
			fuel -= dm;
			if (fuel < 0) {
				dm += fuel;
				fuel = 0;
				rotateAcceleration *= (dm + fuel) / dm;
			}
			// Из закона изменения моента импульса
			double dw = SpaceshipHalfLength * RelativeSpeed * dm / (m*CoeffImpulsMoment);
			if (rotateAcceleration < 0) {
				dw *= (-1);
			}
			w += dw;
		}
	}
	else {
		moveAcceleration = 0;
		rotateAcceleration = 0;
	}
	updateMass();
}

void CSpaceship::SetMoveAcceleration(double value)
{
	moveAcceleration = value;
}

double CSpaceship::GetMoveAcceleration() const
{
	return moveAcceleration;
}

void CSpaceship::SetRotateAcceleration(double value)
{
	rotateAcceleration = value;
}

double CSpaceship::GetRotateAcceleration() const
{
	return rotateAcceleration;
}

double CSpaceship::GetFuel()
{
	return fuel;
}

int CSpaceship::GetMissleCount()
{
	return misslesCount;
}

void CSpaceship::LaunchMissle(CVector& speed)
{
	--misslesCount;
	updateMass();
	this->speed -= (speed * MissleMass / m);
}

int CSpaceship::GetType() const
{
	return CPhysicalObject::SpaceshipType;
}

const double CSpaceship::MaxFuel = 60;
const double CSpaceship::FrontEngineDivider = 5;
const double CSpaceship::SideEngineDivider = 10;
const double CSpaceship::BaseMass = 20;
const double CSpaceship::MissleMass = 2;
const int CSpaceship::MisslesStart = 10;
const double CSpaceship::RelativeSpeed = 100;
const double CSpaceship::ConsumptionSpeed = 3.0;
const double CSpaceship::SpaceshipHalfLength = 16; //r=l/4 // J=m(1/3l*l+1/4*r*r), где l - полудлина корабля
const double CSpaceship::CoeffImpulsMoment = (1 / 3.0 + 1 / 64.0) * SpaceshipHalfLength * SpaceshipHalfLength;

void CSpaceship::updateMass()
{
	m = BaseMass + fuel + MissleMass * misslesCount;
}

void CStar::CreateBrush()
{
	COLORREF starscolor = RGB(200, 200, 0);
	CStar::brush = ::CreateSolidBrush(starscolor);
	CStar::pen = ::CreatePen(PS_SOLID, 1, starscolor);
}

void CStar::FreeBrush()
{
	::DeleteObject(CStar::brush);
	::DeleteObject(CStar::pen);
}

void CStar::InitStarsPaint(HDC paint)
{
	::SelectObject(paint, pen);
	::SelectObject(paint, brush);
}

void CStar::Draw(HDC paint) const
{
	::Ellipse(paint, (int)(location.GetX() - r), (int)(location.GetY() - r), (int)(location.GetX() + r), (int)(location.GetY() + r));
}

int CStar::GetType() const
{
	return CPhysicalObject::StarType;
}

HBRUSH CStar::brush = 0;
HPEN CStar::pen = 0;

CPlanet::CPlanet(int r, int g, int b)
{
	COLORREF color = RGB(r, g, b);
	brush = CreateSolidBrush(color);
	pen = CreatePen(PS_SOLID, 1, color);
}

CPlanet::~CPlanet()
{
	DeleteObject(pen);
	DeleteObject(brush);
}

void CPlanet::Draw(HDC canvas) const
{
	::SelectObject(canvas, pen);
	::SelectObject(canvas, brush);
	::Ellipse(canvas, location.GetX() - r, location.GetY() - r, location.GetX() + r, location.GetY() + r);
}

int CPlanet::GetType() const
{
	return CPhysicalObject::PlanetType;
}

CTarget::CTarget()
{
}

CTarget::~CTarget()
{
}

void CTarget::Draw(HDC paint) const
{
	int x = location.GetX();
	int y = location.GetY();
	::SelectObject(paint, brush);
	::SelectObject(paint, pen);
	::MoveToEx(paint, x - lineR, y, 0);
	::LineTo(paint, x + lineR, y);
	::MoveToEx(paint, x, y - lineR, 0);
	::LineTo(paint, x, y + lineR);
	::Ellipse(paint, x - 2 * R, y - 2 * R, x + 2 * R, y + 2 * R);
}

double CTarget::GetR() const
{
	return 2 * R;
}

int CTarget::GetType() const
{
	return CPhysicalObject::TargetType;
}

void CTarget::CreateBrushes()
{
	brush = ::CreateSolidBrush(RGB(0, 255, 0));
	pen = ::CreatePen(PS_SOLID, R, RGB(255, 5, 0));
}

void CTarget::ClearBrushes()
{
	::DeleteObject(brush);
	::DeleteObject(pen);
}

HBRUSH CTarget::brush = 0;
HPEN CTarget::pen = 0;
const int CTarget::R = 5;
const int CTarget::lineR = 15;


void CFire::Draw(HDC paint) const
{
	int x = location.GetX();
	int y = location.GetY();
	::SelectObject(paint, brush);
	::SelectObject(paint, pen);
	::Ellipse(paint, x - GetR(), y - GetR(), x + GetR(), y + GetR());
}

void CFire::Move(double dt)
{
	t += dt;
}

int CFire::GetType() const
{
	return CPhysicalObject::FireType;
}

CFire::CFire()
{
	t = 0;
	m = 1.0e-10;
}

double CFire::GetR() const
{
	return t*t + 1;
}


void CFire::CreateBrushes()
{
	COLORREF color = RGB(255, 255, 0);
	brush = ::CreateSolidBrush(color);
	pen = ::CreatePen(PS_SOLID, 1, color);
}

void CFire::ClearBrushes()
{
	::DeleteObject(brush);
	::DeleteObject(pen);
}

double CFire::GetT() const
{
	return t;
}

HBRUSH CFire::brush = 0;
HPEN CFire::pen = 0;


void CMissle::Draw(HDC paint) const
{
	int x = location.GetX();
	int y = location.GetY();
	::SelectObject(paint, brush);
	::SelectObject(paint, pen);
	::Ellipse(paint, x - GetR(), y - GetR(), x + GetR(), y + GetR());
}

CMissle::CMissle()
{
	m = 10;
}

double CMissle::GetR() const
{
	return R;
}

int CMissle::GetType() const
{
	return CPhysicalObject::MissleType;
}


void CMissle::CreateBrushes()
{
	COLORREF color = RGB(255, 0, 0);
	brush = ::CreateSolidBrush(color);
	pen = ::CreatePen(PS_SOLID, 1, color);
}

void CMissle::ClearBrushes()
{
	::DeleteObject(brush);
	::DeleteObject(pen);
}

HBRUSH CMissle::brush = 0;
HPEN CMissle::pen = 0;
const int CMissle::R = 3;

