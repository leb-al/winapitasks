#pragma once
#include "stdafx.h"
#include "PhysicalObject.h"

class CSpaceship : public CPhysicalObject {
public:
	CSpaceship();
	virtual ~CSpaceship();

	virtual void SetM(double m) override;

	virtual void Draw(HDC paint) const override;

	virtual void Move(double dt) override;

	void SetMoveAcceleration(double value);

	double GetMoveAcceleration() const;

	void SetRotateAcceleration(double value);

	double GetRotateAcceleration() const;

	double GetFuel();

	int GetMissleCount();
	void LaunchMissle(CVector & speed);

	virtual int GetType() const override;


	static const double MaxFuel;

	static const double BaseMass;
	static const double MissleMass;
	static const int MisslesStart;
	static const double FrontEngineDivider;
	static const double SideEngineDivider;
	static const double SpaceshipHalfLength;
private:
	static const double RelativeSpeed;
	static const double ConsumptionSpeed;
	static const double CoeffImpulsMoment;//J/m;

	double moveAcceleration;
	double rotateAcceleration;
	double fuel;

	int misslesCount;

	void updateMass();
};

class CStar : public CPhysicalObject {
public:
	static void CreateBrush();
	static void FreeBrush();
	static void InitStarsPaint(HDC paint);

	virtual void Draw(HDC paint) const override;

	virtual int GetType() const override;

private:
	static HBRUSH brush;
	static HPEN pen;
};

class CPlanet : public CPhysicalObject {
public:
	CPlanet(int t, int g, int b);
	virtual  ~CPlanet();
	virtual void Draw(HDC canvas) const override;

	virtual int GetType() const override;


private:
	HPEN pen;
	HBRUSH brush;
};

class CTarget : public CPhysicalObject {
public:
	CTarget();
	virtual ~CTarget();

	virtual void Draw(HDC paint)const override;
	virtual double GetR()const override;

	virtual int GetType() const override;

	static void CreateBrushes();
	static void ClearBrushes();
private:
	static HBRUSH brush;
	static HPEN pen;
	static const int R;
	static const int lineR;
};

class CFire : public CPhysicalObject {
public:
	CFire();

	double GetR() const override;

	virtual void Draw(HDC paint) const override;
	
	virtual void Move(double dt) override;
	
	virtual int GetType() const override;
	
	static void CreateBrushes();
	static void ClearBrushes();

	double GetT() const;
private:
	double t;
	static HBRUSH brush;
	static HPEN pen;
};

class CMissle : public CPhysicalObject {
public:
	CMissle();
	virtual void Draw(HDC paint) const override;
	virtual double GetR() const override;

	virtual int GetType() const override;

	static void CreateBrushes();
	static void ClearBrushes();
private:
	static HBRUSH brush;
	static HPEN pen;
	static const int R;

};
