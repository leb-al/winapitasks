
#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN     
#include <windows.h>
#include <CommCtrl.h>

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


#include <vector>
#include <memory>
#include <map>
#include <string>
#include <cmath>
#include <algorithm>
#include <sstream>

#include "resource.h"