#include "stdafx.h"
#include "HeapManager.h"


CHeapManager::CHeapManager() : minSize(0), maxSize(0), created(false)
{
}


CHeapManager::~CHeapManager()
{
	if (created) {
		Destroy();
	}
}

void CHeapManager::Create(int minSize, int maxSize)
{
	if (created) {
		throw std::logic_error("Already created");
	}
	SYSTEM_INFO info;
	::GetSystemInfo(&info);
	pageSize = info.dwPageSize;
	int allocGranularity = info.dwAllocationGranularity;
	this->minSize = roundSize(minSize, pageSize);
	this->maxSize = roundSize(maxSize, allocGranularity);
	begin = ::VirtualAlloc(0, this->maxSize, MEM_RESERVE, PAGE_READWRITE);
	if (begin == 0) {
		throw std::logic_error(ErrorAllocMessage);
	}
	minAllocatedPages = this->minSize / pageSize;
	void * commited = ::VirtualAlloc(begin, this->minSize, MEM_COMMIT, PAGE_READWRITE);
	if (commited != begin) {
		throw std::logic_error(ErrorAllocMessage);
	}
	allocatedPages = minAllocatedPages;
	totalPages = this->maxSize / pageSize;
	pages = new int[totalPages];
	for (int i = 0; i < allocatedPages; i++) {
		pages[i] = 0;
	}
	for (int i = allocatedPages; i < totalPages; i++) {
		pages[i] = -1;
	}
	emptyLarge[0] = this->maxSize;
	created = true;
}

void * CHeapManager::Alloc(int size)
{
	if (!created) {
		throw std::logic_error(ErrorNotExistMessage);
	}
	if (size <= 0) {
		throw std::logic_error("Wrong size");
	}
	size = roundSize(size, MinBlockSize);
	bool isSmall = size < pageSize;
	int addition = roundSize(sizeof(int), MinBlockSize);
	if (isSmall) {
		size += addition;
	}
	int address = searchEmpty(size);
	for (int i = pageByAddress(address); i <= pageByAddress(address + size - 1); i++) {
		if (pages[i] < 0) {
			::VirtualAlloc(convertPointer(i*pageSize), pageSize, MEM_COMMIT, PAGE_READWRITE);
			pages[i] = 0;
			++allocatedPages;
		}
		pages[i]++;
	}
	void* result = convertPointer(isSmall ? address + addition : address);
	if (isSmall) {
		(reinterpret_cast<int*>(result))[-1] = size;
	} else {
		sizes[address] = size;
	}
	return result;
}

void CHeapManager::Free(void * pointer)
{
	if (!created) {
		throw std::logic_error(ErrorNotExistMessage);
	}
	int address = convertAddress(pointer);
	if ((address < 0) || (address > maxSize)) {
		throw std::logic_error("Out of bounds");
	}
	auto iterator = sizes.find(address);
	int size;
	if (iterator != sizes.end()) {
		size = iterator->second;
		sizes.erase(iterator);
	} else {
		size = (reinterpret_cast<int*>(convertPointer(address)))[-1];
		address -= roundSize(sizeof(int), MinBlockSize);
	}

	addEmpty(address, size);
	for (int i = pageByAddress(address); i <= pageByAddress(address + size - 1); i++) {
		pages[i]--;
		if ((pages[i] == 0) && (allocatedPages > minAllocatedPages)) {
			::VirtualAlloc(convertPointer(i*pageSize), pageSize, MEM_DECOMMIT, 0);
			pages[i] = -1;
			--allocatedPages;
		}
	}
}

void CHeapManager::Destroy()
{
	if (!created) {
		throw std::logic_error(ErrorNotExistMessage);
	}
	printAllAllocated();
	sizes.clear();
	emptySmall.clear();
	emptyMedium.clear();
	emptyLarge.clear();
	if (::VirtualFree(begin, 0, MEM_RELEASE) == 0) {
		throw std::logic_error("Fail realising memory");
	}
	begin = 0;
	delete[] pages;
	created = false;
}

int CHeapManager::roundSize(int size, int mod)
{
	return ((size + mod - 1) / mod) * mod;
}

int CHeapManager::convertAddress(void * point)
{
	return reinterpret_cast<char*>(point) - reinterpret_cast<char*>(begin);
}

void * CHeapManager::convertPointer(int address)
{
	return reinterpret_cast<void*>(reinterpret_cast<char*>(begin) + address);
}

int CHeapManager::pageByAddress(int address)
{
	return address / pageSize;
}

void CHeapManager::addEmpty(int address, int size)
{
	int left = serchLeftEmpty(address, emptySmall);
	if (left < 0) {
		left = serchLeftEmpty(address, emptyMedium);
	}
	if (left < 0) {
		left = serchLeftEmpty(address, emptyLarge);
	}
	if (left >= 0) {
		size += address - left;
		address = left;
	}
	int next = address + size;
	if (!checkNotEmpty(next, emptySmall)) {
		size += emptySmall[-next];
		emptySmall.erase(-next);
	} else {
		if (!checkNotEmpty(next, emptyMedium)) {
			size += emptyMedium[-next];
			emptyMedium.erase(-next);
		} else {
			if (!checkNotEmpty(next, emptyLarge)) {
				size += emptyLarge[-next];
				emptyLarge.erase(-next);
			}
		}
	}
	addEmptyMerged(address, size);
}

void CHeapManager::addEmptyMerged(int address, int size)
{
	address = -address;
	if (size < pageSize) {
		emptySmall[address] = size;
	} else {
		if (size < MediumBlockMaxSize) {
			emptyMedium[address] = size;
		} else {
			emptyLarge[address] = size;
		}
	}
}

int CHeapManager::serchLeftEmpty(int address, std::map<int, int>& map)
{
	std::map<int, int>::iterator iter = map.upper_bound(-address);
	if ((iter != map.end()) && ((iter->second - iter->first) == address)) {
		int result = iter->first;
		map.erase(iter);
		return -result;
	} else {
		return -1;
	}
}

int CHeapManager::searchEmpty(int size)
{
	if (size < pageSize) {
		return searchEmptySmall(size);
	} else {
		return ((size < MediumBlockMaxSize) ? searchEmptyMedium(size) : searchEmptyLarge(size));
	}
}

int CHeapManager::searchEmptyBase(int size, std::map<int, int>& map)
{
	for (std::map<int, int>::iterator i = map.begin(); i != map.end(); ++i) {
		int emptySize = i->second;
		if (emptySize >= size) {
			int address = -(i->first);
			map.erase(i);
			if (emptySize > size) {
				addEmptyMerged((address + size), emptySize - size);
			}
			return address;
		}
	}
	return -1;
}

int CHeapManager::searchEmptySmall(int size)
{
	int result = searchEmptyBase(size, emptySmall);
	return ((result < 0) ? searchEmptyMedium(size) : result);
}

int CHeapManager::searchEmptyMedium(int size)
{
	int result = searchEmptyBase(size, emptyMedium);
	return ((result < 0) ? searchEmptyLarge(size) : result);
}

int CHeapManager::searchEmptyLarge(int size)
{
	int result = searchEmptyBase(size, emptyLarge);
	if (result < 0) {
		throw std::logic_error("No free space in heap");
	} else {
		return result;
	}
}

bool CHeapManager::printNotEmpty(std::map<int, int>& map)
{
	bool wasNull = false;
	for (std::map<int, int>::iterator i = map.begin(); i != map.end(); ++i) {
		if (i->first == 0)
			wasNull = true;
		int next = i->second - i->first;
		do {
			next = printAllocedBlock(next);
			if (next < 0) {
				break;
			}
		} while (checkNotEmpty(next, emptySmall) && checkNotEmpty(next, emptyMedium) &&
			checkNotEmpty(next, emptyLarge));
	}
	return wasNull;
}

int CHeapManager::printAllocedBlock(int address)
{
	if (address >= maxSize) {
		return -1;
	}
	void * pointer = convertPointer(address);
	auto iterator = sizes.find(address);
	int size;
	if (iterator != sizes.end()) {
		size = iterator->second;
	} else {
		size = *reinterpret_cast<int*>(convertPointer(address));
	}
	std::cout << "Allocated block " << reinterpret_cast<int>(pointer) <<
		" with size " << size << " and local address " << address << std::endl;
	if (size == 0) {
		return -1;
	}
	return address + size;
}

void CHeapManager::printAllAllocated()
{
	if (!(printNotEmpty(emptySmall) || printNotEmpty(emptyMedium) || printNotEmpty(emptyLarge))) {
		printAllocedBlock(0);
	}
}

bool CHeapManager::checkNotEmpty(int address, std::map<int, int>& map)
{
	return map.find(-address) == map.end();
}

const char* CHeapManager::ErrorNotExistMessage = "Heap not created";
const char* CHeapManager::ErrorAllocMessage = "Can't alloc";
const int CHeapManager::MinBlockSize = 4;
const int CHeapManager::MediumBlockMaxSize = 128 * 4 * 1024;
