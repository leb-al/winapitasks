#pragma once
class CHeapManager {
public:
	CHeapManager();

	~CHeapManager();

	void CHeapManager::Create(int minSize, int maxSize);

	void* CHeapManager::Alloc(int size);

	void CHeapManager::Free(void* mem);

	void CHeapManager::Destroy();

private:
	int minSize;
	int maxSize;
	bool created;
	int pageSize;
	int minAllocatedPages;
	int allocatedPages;
	int totalPages;
	int* pages;
	void* begin;
	std::map<int, int> emptySmall;
	std::map<int, int> emptyMedium;
	std::map<int, int> emptyLarge;
	std::map<int, int> sizes;


	inline int roundSize(int size, int mod);
	inline int convertAddress(void* point);
	inline void* convertPointer(int address);
	inline int pageByAddress(int address);
	void addEmpty(int address, int size);
	void addEmptyMerged(int address, int size);
	int serchLeftEmpty(int address, std::map<int, int>& map);
	int searchEmpty(int size);
	int searchEmptyBase(int size, std::map<int, int>& map);
	int searchEmptySmall(int size);
	int searchEmptyMedium(int size);
	int searchEmptyLarge(int size);
	bool printNotEmpty(std::map<int, int>& map);
	int printAllocedBlock(int address);
	void printAllAllocated();
	bool checkNotEmpty(int address, std::map<int,int>& map);

	static const char* ErrorNotExistMessage;
	static const char* ErrorAllocMessage;
	static const int MinBlockSize;
	static const int MediumBlockMaxSize;
};

