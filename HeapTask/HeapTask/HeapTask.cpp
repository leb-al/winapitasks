#include "stdafx.h"

#include "HeapManager.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

CHeapManager heapManager;

class TestClass {
public:
	TestClass() {}
	~TestClass() {}

	void* operator new(size_t size){
		return heapManager.Alloc(size);
	}

		void* operator new[](size_t size) {
		return heapManager.Alloc(size);
	}

		void operator delete(void* p)
	{
		heapManager.Free(p);
	}

	void operator delete[](void *p) {
		heapManager.Free(p);
	}

	int innerValue;
};

class TestClassStandart {
public:
	TestClassStandart() {}
	~TestClassStandart() {}

	int innerValue;
};

int continiousMy, randomMy, continiousStl, randomStl;

void testContinuous(int n)
{
	std::cout << "Allocate TestClass[" << n << "]" << std::endl;

	clock_t start;
	clock_t finish;

	start = clock();

	TestClass* item = new TestClass();
	item->innerValue = 7;

	TestClass* arrayOfItems = new TestClass[n];
	for (int i = 0; i < n; ++i) {
		arrayOfItems[i].innerValue = i;
	}

	delete item;
	delete[] arrayOfItems;

	finish = clock();
	auto duration1 = finish - start;

	start = clock();

	TestClassStandart* itemStandart = new TestClassStandart();
	itemStandart->innerValue = 7;

	TestClassStandart* arrayOfItemsStandart = new TestClassStandart[n];
	for (int i = 0; i < n; ++i) {
		arrayOfItemsStandart[i].innerValue = i;
	}

	delete itemStandart;
	delete[] arrayOfItemsStandart;

	finish = clock();
	auto duration2 = finish - start;

	continiousMy += duration1;
	continiousStl += duration2;
	std::cout << "HeapManager: " << duration1 << std::endl << "Standart new/delete: " << duration2 << "\n\n";
}

void testRandom(int n)
{
	std::cout << "Allocate " << n << " random blocks" << std::endl;

	std::vector<TestClass*> items(n);
	std::vector<TestClassStandart*> itemsStandart(n);

	clock_t start;
	clock_t finish;

	srand(clock());
	start = clock();
	for (int i = 0; i < n; ++i) {
		items[i] = new TestClass[rand() % 2048];
	}

	for (int i = 0; i < n; ++i) {
		delete[] items[i];
	}

	finish = clock();
	auto duration1 = finish - start;

	srand(0);
	start = clock();
	for (int i = 0; i < n; ++i) {
		itemsStandart[i] = new TestClassStandart[rand() % 2048];
	}
	for (int i = 0; i < n; ++i) {
		delete[] itemsStandart[i];
	}

	finish = clock();
	auto duration2 = finish - start;


	randomMy += duration1;
	randomStl += duration2;
	std::cout << "HeapManager: " << duration1 << std::endl << "Standart new/delete: " << duration2 << "\n\n";
}

int main()
{
	heapManager.Create(20000, 100000000);
	void* p = heapManager.Alloc(50);
	void* p2 = heapManager.Alloc(5);
	heapManager.Alloc(3 * 50);
	heapManager.Free(p);
	heapManager.Free(p2);
	std::cout << "Should be one block" << std::endl;
	heapManager.Destroy();
	std::cout << std::endl;

	heapManager.Create(20000, 100000000);
	continiousMy = 0;
	randomMy = 0;
	continiousStl = 0;
	randomStl = 0;
	for (int i = 0; i < 10; ++i) {
		testContinuous(1000000);
		testRandom(10000);
	}
	std::cout << "HeapManager array " << continiousMy << std::endl;
	std::cout << "Stl array " << continiousStl << std::endl;
	std::cout << "HeapManager random " << randomMy << std::endl;
	std::cout << "Stl random " << randomStl << std::endl;

	int size = 100;

	std::cout << "Should be two blocks" << std::endl;
	void* pointer = heapManager.Alloc(size);
	heapManager.Alloc(2 * size);
	heapManager.Alloc(3 * size);
	heapManager.Free(pointer);
	heapManager.Destroy();

	std::cout << std::endl << "Should be no blocks" << std::endl;
	heapManager.Create(1000, 50000);
	heapManager.Destroy();

	std::cin.get();
	return 0;
}