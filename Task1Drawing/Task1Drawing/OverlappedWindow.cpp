#include "stdafx.h"
#include "OverlappedWindow.h"

COverlappedWindow::COverlappedWindow()
{
}


COverlappedWindow::~COverlappedWindow()
{
}

bool COverlappedWindow::RegisterClass()
{
	WNDCLASSEX windowClassInforamtion;

	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);

	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = sizeof(long);
	HMODULE instance = GetModuleHandleW(nullptr);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_TASK1DRAWING));
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	windowClassInforamtion.lpszMenuName = 0;
	windowClassInforamtion.lpszClassName = COverlappedWindow::ClassName;
	windowClassInforamtion.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_SMALL));

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}



bool COverlappedWindow::Create()
{
	HWND handle_ = CreateWindowEx(0, COverlappedWindow::ClassName, L"Lebedev Aleksey Window", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 400, 300, 0, 0, GetModuleHandleW(nullptr), this);
	this->handle = handle_;
	return (handle != 0);
}

void COverlappedWindow::Show(int cmdShow)
{
	ShowWindow(handle, cmdShow);
}

void COverlappedWindow::OnDestroy()
{
	PostQuitMessage(0);
	// ���������� ��� ������ ��� ������� � ������������, ��� ��� ��������.
	//::MessageBox(0, L"I am destroed", L"DEBUG", MB_OK);
}

const wchar_t* COverlappedWindow::ClassName = L"COverlappedWindow";

LRESULT __stdcall COverlappedWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLong(handle, 0, reinterpret_cast<LONG>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	COverlappedWindow* wndPtr = reinterpret_cast<COverlappedWindow*>(GetWindowLong(handle, 0));
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

