#include "stdafx.h"
#include "OverlappedWindow.h"


COverlappedWindow::COverlappedWindow()
{
	bitmap = 0;
	bitmapContext = 0;
	pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
	brush = CreateSolidBrush(RGB(0, 255, 0));

}


COverlappedWindow::~COverlappedWindow()
{
}

bool COverlappedWindow::RegisterClass()
{
	WNDCLASSEX windowClassInforamtion;

	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);

	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 3 * sizeof(long);
	HMODULE instance = GetModuleHandleW(nullptr);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_TASK1DRAWING));
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = 0;
	windowClassInforamtion.lpszMenuName = 0;
	windowClassInforamtion.lpszClassName = COverlappedWindow::ClassName;
	windowClassInforamtion.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_SMALL));

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}



bool COverlappedWindow::Create()
{
	HWND handle_ = CreateWindowEx(0, COverlappedWindow::ClassName, L"Lebedev Aleksey Window", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 500, 400, 0, 0, GetModuleHandleW(nullptr), this);
	this->handle = handle_;
	startTime = GetTickCount();
	return (handle != 0);
}

void COverlappedWindow::Show(int cmdShow)
{
	ShowWindow(handle, cmdShow);
	timer = SetTimer(handle, DrawTimerID, 10, 0);
	if (timer == 0) {
		PostQuitMessage(5);
		::MessageBox(handle, L"I am destroed", L"DEBUG", MB_ICONERROR);
	}
}

void COverlappedWindow::OnDestroy()
{
	KillTimer(handle, timer);
	DeleteObject(brush);
	DeleteObject(pen);
	destroyDoubleBuffer();

	// ���������� ��� ������ ��� ������� � ������������, ��� ��� ��������.
	//::MessageBox(0, L"I am destroed", L"DEBUG", MB_OK);

	PostQuitMessage(0);
}

const wchar_t* COverlappedWindow::ClassName = L"COverlappedWindow";
const int COverlappedWindow::DrawTimerID = 0;
const int COverlappedWindow::CircleR = 50;
const int COverlappedWindow::CircleTraectoryR = 100;
const float COverlappedWindow::CircleSpeed = 180 * 20 / 3.14159265358;

LRESULT __stdcall COverlappedWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLong(handle, 0, reinterpret_cast<LONG>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	COverlappedWindow* wndPtr = reinterpret_cast<COverlappedWindow*>(GetWindowLong(handle, 0));
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_PAINT:
			wndPtr->onPaint();
			return 0;
		case WM_TIMER:
			wndPtr->drawTimerTick();
			return 0;
		case WM_SIZING:
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->resize(reinterpret_cast<RECT*>(lParam));
			return result;
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}

	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void COverlappedWindow::onPaint()
{
	PAINTSTRUCT paintStruct;
	HDC paintDC = ::BeginPaint(handle, &paintStruct);

	RECT rect;
	::GetClientRect(handle, &rect);

	drawContent(bitmapContext, bitmapWidth, bitmapHeight);
	BOOL code = BitBlt(paintDC, 0, 0, bitmapWidth, bitmapHeight, bitmapContext, 0, 0, SRCCOPY);
	if (!code) {
		PostQuitMessage(4);
	}

	::EndPaint(handle, &paintStruct);
}

void COverlappedWindow::drawContent(HDC paintDC, const int width, const int height)
{
	RECT clientContext;
	clientContext.bottom = height;
	clientContext.right = width;
	clientContext.left = 0;
	clientContext.top = 0;
	FillRect(paintDC, &clientContext, reinterpret_cast<HBRUSH>(COLOR_WINDOW));
	int t = GetTickCount() - startTime;
	int x = width / 2 + cos(4 * t / CircleSpeed) * CircleTraectoryR;
	int y = height / 2 + sin(3 * t / CircleSpeed) * CircleTraectoryR;
	::SelectObject(paintDC, pen);
	::SelectObject(paintDC, brush);
	Ellipse(paintDC, x - CircleR, y - CircleR, x + CircleR, y + CircleR);
}

void COverlappedWindow::drawTimerTick()
{
	RECT rect;
	::GetClientRect(handle, &rect);
	InvalidateRect(handle, &rect, true);
}

void COverlappedWindow::resize(const RECT* area)
{
	destroyDoubleBuffer();
	HDC tmp = GetDC(handle);
	bitmapContext = CreateCompatibleDC(tmp);
	bitmapWidth = area->right - area->left + 1;
	bitmapHeight = area->bottom - area->top + 1;
	bitmap = CreateCompatibleBitmap(tmp, bitmapWidth, bitmapHeight);
	SelectObject(bitmapContext, bitmap);
	ReleaseDC(handle, tmp);
}


void COverlappedWindow::destroyDoubleBuffer()
{
	if ((bitmapContext != 0) || (bitmap != 0)) {
		DeleteDC(bitmapContext);
		DeleteObject(bitmap);
	}
}
