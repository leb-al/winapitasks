#pragma once
class COverlappedWindow {
public:
	COverlappedWindow();
	~COverlappedWindow();

	// ���������������� ����� ����
	static bool RegisterClass();

	// ������� ��������� ����
	bool Create();
	// �������� ����
	void Show(int cmdShow);

protected:
	void OnDestroy();

private:
	HWND handle; // ����� ����
	DWORD startTime;
	UINT_PTR timer;
	HBITMAP bitmap;
	HDC bitmapContext;
	HPEN pen;
	HBRUSH brush;
	int bitmapWidth;
	int bitmapHeight;

	// ��� ������
	static const wchar_t* ClassName;
	// ID �������
	static const int DrawTimerID;
	// ������ �����
	static const int CircleR;
	// ������ ���������� �������� �����
	static const int CircleTraectoryR;
	// �������� ������� �������� ����� (�������� � ��������)
	static const float CircleSpeed;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

	void onPaint();

	void drawContent(HDC paintDC, const int width, const int height);

	void drawTimerTick();

	void resize(const RECT* area);
	void destroyDoubleBuffer();
};


