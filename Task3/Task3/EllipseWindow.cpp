#include "stdafx.h"
#include "EllipseWindow.h"


CEllipseWindow::CEllipseWindow() :
	CEllipseWindow(1, 1)
{
}


CEllipseWindow::CEllipseWindow(int n, int m) :
	xCoeff(n), yCoeff(m)
{
	bitmap = 0;
	bitmapContext = 0;

}

CEllipseWindow::~CEllipseWindow()
{
}

bool CEllipseWindow::RegisterClass()
{
	WNDCLASSEX windowClassInforamtion;

	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);

	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 3 * sizeof(long);
	HMODULE instance = GetModuleHandleW(nullptr);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_TASK3));
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = 0;
	windowClassInforamtion.lpszMenuName = 0;
	windowClassInforamtion.lpszClassName = CEllipseWindow::ClassName;
	windowClassInforamtion.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_SMALL));

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}

void CEllipseWindow::ClearResources()
{
	DeleteObject(brush);
	DeleteObject(pen);
	DeleteObject(focusedBrush);
}

bool CEllipseWindow::Create(HWND parent)
{
	handle = CreateWindowEx(0, CEllipseWindow::ClassName, L"Lebedev Aleksey Window", WS_CHILDWINDOW,
		0, 0, 500, 400, parent, 0, GetModuleHandleW(nullptr), this);
	startTime = GetTickCount();
	return (handle != 0);
}

void CEllipseWindow::Show(int cmdShow)
{
	ShowWindow(handle, cmdShow);
	timer = SetTimer(handle, DrawTimerID, 10, 0);
	if (timer == 0) {
		::MessageBox(handle, L"I am destroed", L"DEBUG", MB_ICONERROR);
		PostQuitMessage(5);
	}
}

HWND CEllipseWindow::GetHandle()
{
	return handle;
}

void CEllipseWindow::OnDestroy()
{
	KillTimer(handle, timer);
	DeleteObject(brush);
	DeleteObject(pen);
	DeleteObject(focusedBrush);
	destroyDoubleBuffer();

	// ���������� ��� ������ ��� ������� � ������������, ��� ��� ��������.
	//::MessageBox(0, L"I am destroed", L"DEBUG", MB_OK);
}

HPEN CEllipseWindow::pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
HBRUSH CEllipseWindow::brush = CreateSolidBrush(RGB(0, 255, 0));
HBRUSH CEllipseWindow::focusedBrush = CreateSolidBrush(RGB(0, 0, 255));

const wchar_t* CEllipseWindow::ClassName = L"CEllipseWindow";
const int CEllipseWindow::DrawTimerID = 0;
const int CEllipseWindow::CircleR = 25;
const int CEllipseWindow::CircleTraectoryR = 50;
const float CEllipseWindow::CircleSpeed = 180 * 20 / 3.14159265358;

LRESULT __stdcall CEllipseWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLong(handle, 0, reinterpret_cast<LONG>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	CEllipseWindow* wndPtr = reinterpret_cast<CEllipseWindow*>(GetWindowLong(handle, 0));
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_PAINT:
			wndPtr->onPaint();
			return 0;
		case WM_TIMER:
			wndPtr->drawTimerTick();
			return 0;
		case WM_SIZING:
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->resize(reinterpret_cast<RECT*>(lParam));
			return result;
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_LBUTTONDOWN:
			::SetFocus(handle);
			break;
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void CEllipseWindow::onPaint()
{
	PAINTSTRUCT paintStruct;
	HDC paintDC = ::BeginPaint(handle, &paintStruct);

	RECT rect;
	::GetClientRect(handle, &rect);

	drawContent(bitmapContext, bitmapWidth, bitmapHeight);
	BOOL code = BitBlt(paintDC, 0, 0, bitmapWidth, bitmapHeight, bitmapContext, 0, 0, SRCCOPY);
	if (!code) {
		::MessageBox(handle, L"Bad paint", L"DEBUG", MB_ICONERROR);
		PostQuitMessage(4);
	}

	::EndPaint(handle, &paintStruct);
}

void CEllipseWindow::drawContent(HDC paintDC, const int width, const int height)
{
	RECT clientContext;
	clientContext.bottom = height;
	clientContext.right = width;
	clientContext.left = 0;
	clientContext.top = 0;
	FillRect(paintDC, &clientContext, reinterpret_cast<HBRUSH>(COLOR_WINDOW));
	int t = GetTickCount() - startTime;
	int x = width / 2 + cos(xCoeff * t / CircleSpeed) * CircleTraectoryR;
	int y = height / 2 + sin(yCoeff * t / CircleSpeed) * CircleTraectoryR;
	::SelectObject(paintDC, pen);
	if (handle == ::GetFocus())
		::SelectObject(paintDC, focusedBrush);
	else
		::SelectObject(paintDC, brush);
	Ellipse(paintDC, x - CircleR, y - CircleR, x + CircleR, y + CircleR);
}

void CEllipseWindow::drawTimerTick()
{
	RECT rect;
	::GetClientRect(handle, &rect);
	InvalidateRect(handle, &rect, true);
}

void CEllipseWindow::resize(const RECT* area)
{
	destroyDoubleBuffer();
	HDC tmp = GetDC(handle);
	bitmapContext = CreateCompatibleDC(tmp);
	bitmapWidth = area->right - area->left + 1;
	bitmapHeight = area->bottom - area->top + 1;
	bitmap = CreateCompatibleBitmap(tmp, bitmapWidth, bitmapHeight);
	SelectObject(bitmapContext, bitmap);
	ReleaseDC(handle, tmp);
}


void CEllipseWindow::destroyDoubleBuffer()
{
	if ((bitmapContext != 0) || (bitmap != 0)) {
		DeleteDC(bitmapContext);
		DeleteObject(bitmap);
	}
}
