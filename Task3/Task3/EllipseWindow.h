#pragma once
class CEllipseWindow {
public:
	CEllipseWindow();
	CEllipseWindow(int n, int m);
	~CEllipseWindow();

	// ���������������� ����� ����
	static bool RegisterClass();

	static void ClearResources();

	// ������� ��������� ����
	bool Create(HWND parent);
	// �������� ����
	void Show(int cmdShow);

	HWND GetHandle();

protected:
	void OnDestroy();

private:
	HWND handle; // ����� ����
	DWORD startTime;
	UINT_PTR timer;
	HBITMAP bitmap;
	HDC bitmapContext;
	static HPEN pen;
	static HBRUSH brush;
	static HBRUSH focusedBrush;
	int bitmapWidth;
	int bitmapHeight;
	int xCoeff;
	int yCoeff;

	// ��� ������
	static const wchar_t* ClassName;
	// ID �������
	static const int DrawTimerID;
	// ������ �����
	static const int CircleR;
	// ������ ���������� �������� �����
	static const int CircleTraectoryR;
	// �������� ������� �������� ����� (�������� � ��������)
	static const float CircleSpeed;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

	void onPaint();

	void drawContent(HDC paintDC, const int width, const int height);

	void drawTimerTick();

	void resize(const RECT* area);
	void destroyDoubleBuffer();
};


