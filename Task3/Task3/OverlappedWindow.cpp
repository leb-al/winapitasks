#include "stdafx.h"
#include "OverlappedWindow.h"


COverlappedWindow::COverlappedWindow()
{
	for (size_t i = 0; i < ChidrenCount; i++) {
		children.push_back(CEllipseWindow(i + 1, i + 2));
	}
}

COverlappedWindow::~COverlappedWindow()
{
}

bool COverlappedWindow::RegisterClass()
{
	WNDCLASSEX windowClassInforamtion;

	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);

	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 3 * sizeof(long);
	HMODULE instance = GetModuleHandleW(nullptr);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_TASK3));
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = 0;
	windowClassInforamtion.lpszMenuName = 0;
	windowClassInforamtion.lpszClassName = COverlappedWindow::ClassName;
	windowClassInforamtion.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_SMALL));

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}



bool COverlappedWindow::Create()
{
	HWND handle_ = CreateWindowEx(0, COverlappedWindow::ClassName, L"Lebedev Aleksey Window", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 1000, 600, 0, 0, GetModuleHandleW(nullptr), this);
	this->handle = handle_;
	for (size_t i = 0; i < ChidrenCount; i++) {
		children[i].Create(handle_);
	}
	return (handle != 0);
}

void COverlappedWindow::Show(int cmdShow)
{
	ShowWindow(handle, cmdShow);
	for (size_t i = 0; i < ChidrenCount; i++) {
		children[i].Show(SW_SHOW);
		//TODO
	}
}

void COverlappedWindow::OnDestroy()
{
	// ���������� ��� ������ ��� ������� � ������������, ��� ��� ��������.
	//::MessageBox(0, L"I am destroed", L"DEBUG", MB_OK);

	PostQuitMessage(0);
}

const wchar_t* COverlappedWindow::ClassName = L"COverlappedWindow";
const int COverlappedWindow::ChidrenCount = 4;

LRESULT __stdcall COverlappedWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLong(handle, 0, reinterpret_cast<LONG>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	COverlappedWindow* wndPtr = reinterpret_cast<COverlappedWindow*>(GetWindowLong(handle, 0));
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_SIZING:
			result = DefWindowProc(handle, message, wParam, lParam);
			wndPtr->resize(reinterpret_cast<RECT*>(lParam));
			return result;
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->resize(&area);
			return result;
		}
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void COverlappedWindow::resize(const RECT* area)
{
	if (children.size() == 0)
		return;
	int width = area->right - area->left;
	int height = area->bottom - area->top;
	SetWindowPos(children[0].GetHandle(), HWND_TOP, area->left, area->top, width / 2, height / 2, 0);
	SetWindowPos(children[1].GetHandle(), HWND_TOP, area->left + width / 2, area->top, width / 2, height / 2, 0);
	SetWindowPos(children[2].GetHandle(), HWND_TOP, area->left, area->top + height / 2, width / 2, height / 2, 0);
	SetWindowPos(children[3].GetHandle(), HWND_TOP, area->left + width / 2, area->top + height / 2, width / 2, height / 2, 0);
}
