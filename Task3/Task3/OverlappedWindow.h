#pragma once
#include "EllipseWindow.h"

class COverlappedWindow {
public:
	COverlappedWindow();
	~COverlappedWindow();

	// ���������������� ����� ����
	static bool RegisterClass();

	// ������� ��������� ����
	bool Create();
	// �������� ����
	void Show(int cmdShow);

protected:
	void OnDestroy();

private:
	HWND handle; // ����� ����
	std::vector<CEllipseWindow> children;

	// ��� ������
	static const wchar_t* ClassName;
	static const int ChidrenCount;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

	void resize(const RECT* area);
};


