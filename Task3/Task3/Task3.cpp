// Task1Drawing.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Task3.h"
#include "OverlappedWindow.h"
#include "EllipseWindow.h"

int WINAPI wWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPWSTR    lpCmdLine,
	int       nCmdShow)
{
	if (!COverlappedWindow::RegisterClassW()) {
		return 1;
	}
	if (!CEllipseWindow::RegisterClassW()) {
		return 1;
	}
	COverlappedWindow window;
	if (!window.Create()) {
		return 3;
	}
	window.Show(nCmdShow);

	MSG message;
	BOOL getMessageResult = 0;
	while ((getMessageResult = ::GetMessage(&message, 0, 0, 0)) != 0) {
		if (getMessageResult == -1) {
			return 2;
		}
		::TranslateMessage(&message);
		::DispatchMessage(&message);
	}

	CEllipseWindow::ClearResources();

	return 0;
}



