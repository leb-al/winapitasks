#include "stdafx.h"
#include "OverlappedWindow.h"
#include "Resource.h"
#include <commdlg.h>
#include <sstream>
#include "notepadlib.h"

#define MAX_TITLE_LENGTH 20

COverlappedWindow::COverlappedWindow()
{
	changes = false;
	dialog = 0;
	brush = 0;
	wordCountModule = 0;
	processesClearers.emplace_back();
	processesClearers.emplace_back();
	processesClearers.emplace_back();
	processesClearers.emplace_back();
	for (size_t i = 0; i < processesClearers.size(); i++) {
		ZeroMemory(&processesClearers[i], sizeof(PROCESS_INFORMATION));
		inputEvents.push_back(0);
		outputEvents.push_back(0);
	}
}

COverlappedWindow::~COverlappedWindow()
{
}

bool COverlappedWindow::RegisterClass()
{
	HMODULE instance = GetModuleHandleW(nullptr);

	WNDCLASSEX windowClassInforamtion;
	windowClassInforamtion.cbSize = sizeof(WNDCLASSEX);
	windowClassInforamtion.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	windowClassInforamtion.lpfnWndProc = windowProc;
	windowClassInforamtion.cbClsExtra = 0;
	windowClassInforamtion.cbWndExtra = 3 * sizeof(long);
	windowClassInforamtion.hInstance = instance;
	windowClassInforamtion.hIcon = (HICON)::LoadImage(instance, MAKEINTRESOURCE(IDI_TASK4), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	windowClassInforamtion.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassInforamtion.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
	windowClassInforamtion.lpszMenuName = MAKEINTRESOURCE(IDC_TASK4);
	windowClassInforamtion.lpszClassName = COverlappedWindow::ClassName;
	windowClassInforamtion.hIconSm = (HICON)::LoadImage(instance, MAKEINTRESOURCE(IDI_TASK4), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);

	return (::RegisterClassEx(&windowClassInforamtion) == 0 ? false : true);
}



bool COverlappedWindow::Create()
{
	HINSTANCE instance = GetModuleHandleW(nullptr);
	wchar_t title[MAX_TITLE_LENGTH];
	::LoadString(instance, IDS_APP_TITLE, title, MAX_TITLE_LENGTH);
	this->handle = CreateWindowEx(WS_EX_LAYERED, COverlappedWindow::ClassName, title, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 1000, 600, 0, 0, instance, this);
	if (this->handle == 0)
		return false;
	child = CreateWindowEx(0,
		L"EDIT", 0,
		ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_CHILD |
		WS_VISIBLE | WS_VSCROLL | WS_HSCROLL, 0, 0, 100, 100, this->handle, 0, instance, 0);
	if (child == 0)
		return false;
	HRSRC textResource = ::FindResource(instance, MAKEINTRESOURCE(IDR_DEFAULTTEXT), L"BINARY");
	HGLOBAL textGlobal = ::LoadResource(instance, textResource);
	LPVOID text = ::LockResource(textGlobal);
	::SetWindowText(child, reinterpret_cast<LPCWSTR>(text));
	return true;
}

void COverlappedWindow::Show(int cmdShow)
{
	::ShowWindow(handle, cmdShow);
	ApplySettings();
	::SetFocus(child);
}

HWND COverlappedWindow::GetHandle()
{
	return handle;
}

CSettings& COverlappedWindow::GetSettings()
{
	return settings;
}

COverlappedWindow * COverlappedWindow::GetWindowByHandle(HWND handle)
{
	return reinterpret_cast<COverlappedWindow*>(GetWindowLong(handle, 0));
}

void COverlappedWindow::ApplySettings()
{
	HFONT font = reinterpret_cast<HFONT>(::SendMessage(child, WM_GETFONT, 0, 0));
	if (font == 0) {
		font = reinterpret_cast<HFONT>(::GetStockObject(DEFAULT_GUI_FONT));
	}
	LOGFONT fontinfo;
	::GetObject(font, sizeof(LOGFONT), &fontinfo);
	::DeleteObject(font);
	fontinfo.lfHeight = settings.GetFontSize();
	font = ::CreateFontIndirect(&fontinfo);
	::SendMessage(child, WM_SETFONT, reinterpret_cast<WPARAM>(font), TRUE);
	::DeleteObject(font);
	::DeleteObject(reinterpret_cast<HGDIOBJ>(font));

	RECT area;
	::GetClientRect(handle, &area);
	::InvalidateRect(handle, &area, false);
	::SetLayeredWindowAttributes(handle, RGB(0, 1, 0), settings.GetOpacity(), LWA_ALPHA);
}

HWND COverlappedWindow::GetDialog()
{
	return dialog;
}

void COverlappedWindow::OnDestroy()
{
	// ���������� ��� ������ ��� ������� � ������������, ��� ��� ��������.
	//::MessageBox(0, L"I am destroed", L"DEBUG", MB_OK);
	if (wordCountModule != 0) {
		FreeLibrary(wordCountModule);
	}

	HANDLE finishEvent = ::OpenEvent(EVENT_ALL_ACCESS, false, L"Global\\NotepadTerminate");
	if (finishEvent != 0) {
		::SetEvent(finishEvent);
	}

	for (size_t i = 0; i < processesClearers.size(); i++) {
		CloseHandle(processesClearers[i].hProcess);
		CloseHandle(inputEvents[i]);
		CloseHandle(outputEvents[i]);
	}
	CloseHandle(finishEvent);

	PostQuitMessage(0);
}

const wchar_t* COverlappedWindow::ClassName = L"COverlappedWindow";
const wchar_t* COverlappedWindow::ErrorTitle = L"Error";
const int COverlappedWindow::ChidrenCount = 4;

LRESULT __stdcall COverlappedWindow::windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE) {
		SetWindowLong(handle, 0, reinterpret_cast<LONG>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams));
		return DefWindowProc(handle, message, wParam, lParam);
	}

	COverlappedWindow* wndPtr = GetWindowByHandle(handle);
	LRESULT result;
	switch (message) {
		case WM_DESTROY:
			wndPtr->OnDestroy();
			break;
		case WM_SIZING:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->onResize(&area);
			return result;
		}
		case WM_SIZE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->onResize(&area);
			return result;
		}
		case WM_CREATE:
		{
			result = DefWindowProc(handle, message, wParam, lParam);
			RECT area;
			::GetClientRect(handle, &area);
			wndPtr->onResize(&area);
			return result;
		}
		case WM_CLOSE:
			if (wndPtr->onClose()) {
				return DefWindowProc(handle, message, wParam, lParam);
			} else {
				return 0;
			}
		case WM_COMMAND:
			wndPtr->onCommandRecieved(wParam, lParam);
			break;
		case WM_CTLCOLOREDIT:
			return wndPtr->onCtlColorEdit(handle, wParam, lParam);
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

void COverlappedWindow::onResize(const RECT* area)
{
	SetWindowPos(child, HWND_TOP, area->left, area->top, area->right, area->bottom, 0);
}

bool COverlappedWindow::onClose()
{
	if (this->changes) {
		int result = ::MessageBoxW(this->handle, L"���� ������������� ���������. ��������� ��?",
			L"��������� ���������?", MB_YESNOCANCEL | MB_ICONQUESTION);
		switch (result) {
			case IDCANCEL:
				return false;
			case IDYES:
				return save();
			case IDNO:
				return true;
			default:
				return false;
		}
	} else {
		return true;
	}
}

bool COverlappedWindow::writeFile(bool result, HANDLE file, wchar_t* text, int len)
{
	DWORD writtenBytes;
	if (result && (!::WriteFile(file, text, len * sizeof(wchar_t), &writtenBytes, 0)) && (writtenBytes == len)) {
		::MessageBoxW(this->handle, L"������ ���������� ��� ������ �����.", L"������", MB_OK | MB_ICONERROR);
		result = false;
	}
	return result;
}

bool COverlappedWindow::save()
{
	OPENFILENAMEW filename;
	filename.lStructSize = sizeof(filename);
	filename.hwndOwner = handle;
	filename.hInstance = GetModuleHandleW(nullptr);
	filename.lpstrFilter = L"TextFiles (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0";
	filename.lpstrCustomFilter = 0;
	filename.nFilterIndex = 1;
	filename.nMaxFile = 1024;
	filename.lpstrFile = new wchar_t[filename.nMaxFile];
	filename.lpstrFile[0] = 0;
	filename.lpstrInitialDir = 0;
	filename.lpstrTitle = L"���������";
	filename.nMaxFileTitle = 0;
	filename.lpstrDefExt = L".txt";
	filename.lCustData = 0;
	filename.lpfnHook = 0;
	filename.lpTemplateName = 0;
	filename.nFileOffset = 0;
	filename.nFileExtension = 0;
	filename.Flags = OFN_FILEMUSTEXIST;


	bool result = ::GetSaveFileName(&filename);
	if (result) {
		int textLen = ::GetWindowTextLength(child);
		wchar_t* text = new wchar_t[textLen + 1];
		::GetWindowText(child, text, textLen + 1);
		HANDLE file = ::CreateFile(filename.lpstrFile, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		if (file == INVALID_HANDLE_VALUE) {
			::MessageBoxW(this->handle, L"������ ���������� ��� �������� �����", L"������", MB_OK | MB_ICONERROR);
			result = false;
		}
		wchar_t bomBuffer[2];
		int bomBufferSize;
		if (sizeof(wchar_t) == 1) {
			bomBufferSize = 2;
			bomBuffer[0] = 255;
			bomBuffer[1] = 254;
		} else {
			bomBufferSize = 1;
			bomBuffer[0] = 254 * 256 + 255;
		}
		result = writeFile(result, file, bomBuffer, bomBufferSize);
		result = writeFile(result, file, text, textLen);
		::CloseHandle(file);
		delete[] text;
	}

	delete[] filename.lpstrFile;
	return result;
}


bool COverlappedWindow::open()
{
	OPENFILENAMEW filename;
	filename.lStructSize = sizeof(filename);
	filename.hwndOwner = handle;
	filename.hInstance = GetModuleHandleW(nullptr);
	filename.lpstrFilter = L"TextFiles (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0";
	filename.lpstrCustomFilter = 0;
	filename.nFilterIndex = 1;
	filename.nMaxFile = 1024;
	filename.lpstrFile = new wchar_t[filename.nMaxFile];
	filename.lpstrFile[0] = 0;
	filename.lpstrInitialDir = 0;
	filename.lpstrTitle = L"�������";
	filename.nMaxFileTitle = 0;
	filename.lpstrDefExt = L".txt";
	filename.lCustData = 0;
	filename.lpfnHook = 0;
	filename.lpTemplateName = 0;
	filename.nFileOffset = 0;
	filename.nFileExtension = 0;
	filename.Flags = OFN_FILEMUSTEXIST;

	bool result = ::GetOpenFileName(&filename);
	HANDLE file;
	if (result) {
		file = ::CreateFile(filename.lpstrFile, GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE) {
			::MessageBoxW(this->handle, L"������ �������� �����", L"������", MB_OK | MB_ICONERROR);
			result = false;
		}
	}
	if (result) {
		DWORD textLen = ::GetFileSize(file, 0);
		DWORD bytesRead;
		wchar_t* buffer = new wchar_t[textLen / sizeof(wchar_t) + 1];
		::ReadFile(file, buffer, textLen*sizeof(wchar_t), &bytesRead, NULL);
		buffer[textLen / sizeof(wchar_t)] = 0;
		::SendMessage(child, WM_SETTEXT, textLen + 1, reinterpret_cast<LPARAM>(buffer));
		delete[]buffer;
		changes = false;
		::CloseHandle(file);
	}
	delete[] filename.lpstrFile;
	return result;
}


typedef int(*WordCountMethod)(const wchar_t* pointer);

void COverlappedWindow::onCommandRecieved(WPARAM wParam, LPARAM lParam)
{
	if (HIWORD(wParam) == EN_CHANGE) {
		changes = true;
		return;
	}
	HINSTANCE instance = GetModuleHandleW(nullptr);
	switch (LOWORD(wParam)) {
		case IDM_FILE_SAVE:
			save();
			break;
		case IDM_FILE_OPEN:
			open();
			break;
		case IDA_QUIT:
		case IDM_EXIT:
			::SendMessage(handle, WM_CLOSE, 0, 0);
			break;
		case IDM_SETTING:
		{
			INT_PTR result = ::DialogBox(instance, MAKEINTRESOURCE(IDD_SETTING), handle, CSettings::DialogProc);
			break;
		}
		case IDM_VIEW_SETTINGSNOTMODAL:
		{
			dialog = ::CreateDialog(instance, MAKEINTRESOURCE(IDD_SETTING), handle, CSettings::DialogProc);
			::ShowWindow(dialog, SW_SHOW);
			::EnableMenuItem(::LoadMenu(instance, MAKEINTRESOURCE(IDC_TASK4)), 1, MF_DISABLED | MF_BYPOSITION);
			break;
		}
		case IDM_WORD_COUNT:
		{
			int textLen = ::GetWindowTextLength(child);
			wchar_t* text = new wchar_t[textLen + 1];
			::GetWindowText(child, text, textLen + 1);
			showWordCount(WordsCount(text));
			delete[] text;
			break;
		}
		case IDM_WORD_COUNT_DYNAMIC:
		{
			if (wordCountModule == 0) {
				wordCountModule = ::LoadLibrary(L"notepadlib");
				if (wordCountModule == 0) {
					::MessageBox(handle, L"Can't load library", L"Error", MB_OK);
					return;
				}
			}
			WordCountMethod wordCount = reinterpret_cast<WordCountMethod>(::GetProcAddress(wordCountModule, "WordsCount"));
			if (wordCount == 0) {
				::MessageBox(handle, L"Can't load method", L"Error", MB_OK);
				return;
			}
			int textLen = ::GetWindowTextLength(child);
			wchar_t* text = new wchar_t[textLen + 1];
			::GetWindowText(child, text, textLen + 1);
			showWordCount(wordCount(text));
			delete[] text;
			break;
		}
		case IDM_EDIT_DICTIONARYCLEAR:
			clearWords();
		default:
			break;
	}
}

int COverlappedWindow::splitBySpace(wchar_t* buffer, int index, int length)
{
	wchar_t* position = buffer + index;
	while ((index < length) && (!iswspace(*position))) {
		++position;
		++index;
	}
	return index;
}

void COverlappedWindow::createMap(wchar_t* text, int position, int length, int processNum)
{
	int size = length + sizeof(int) + 1;
	std::wstring filename = L"Local\\NotapadTextPart" + std::to_wstring(processesClearers[processNum].dwProcessId);
	HANDLE file = CreateFileMapping(INVALID_HANDLE_VALUE, 0, PAGE_READWRITE, HIWORD(size), LOWORD(size), filename.c_str());

	if (file == 0) {
		MessageBox(0, L"Can't create file. Can't create map", ErrorTitle, MB_ICONERROR);
		return;
	}

	int* sizeBuffer = reinterpret_cast<int*> (MapViewOfFile(file, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(int)));
	if (sizeBuffer == 0) {
		MessageBox(0, L"Can't open file. Can't set size", ErrorTitle, MB_ICONERROR);
		return;
	}
	*sizeBuffer = size;
	wchar_t* buffer = reinterpret_cast<wchar_t*> (reinterpret_cast<char*>(
		MapViewOfFile(file, FILE_MAP_ALL_ACCESS, 0, 0, size)) + sizeof(int));
	if (buffer == 0) {
		MessageBox(0, L"Can't open file. Can't set content", ErrorTitle, MB_ICONERROR);
		return;
	}

	const char* begin = reinterpret_cast<const char*>(text + position);
	const char* end = reinterpret_cast<const char*>(text + position + length);
	size = end - begin;
	memcpy(buffer, begin, end - begin);
	buffer[size] = 0;
	MessageBox(0, reinterpret_cast<wchar_t*>(buffer), L"buffer", MB_OK);

	SetEvent(outputEvents[processNum]);
}

void COverlappedWindow::clearWords()
{
	runProcesses();
	int textLen = ::GetWindowTextLength(child);
	wchar_t* text = new wchar_t[textLen + 1];
	::GetWindowText(child, text, textLen + 1);

	std::vector<int> slitters;
	slitters.push_back(0);
	int count = processesClearers.size();
	for (size_t i = 0; i < count; i++) {
		slitters.push_back(splitBySpace(text, (i + 1)*textLen / count, textLen));
		createMap(text, slitters[i], slitters[i + 1] - slitters[i], i);
	}
	delete[] text;
}

LRESULT COverlappedWindow::onCtlColorEdit(HWND handle, WPARAM wParam, LPARAM lParam)
{
	COLORREF& backColor = settings.GetBackground();
	HDC context = reinterpret_cast<HDC>(wParam);
	::SetBkColor(context, backColor);
	::SetTextColor(context, settings.GetForeground());
	if (brush != 0) {
		::DeleteObject(brush);
	}
	brush = CreateSolidBrush(backColor);
	return reinterpret_cast<LRESULT>(brush);
}

void COverlappedWindow::showWordCount(int count)
{
	std::wstringstream text;
	text << "Words count: " << count;
	::MessageBox(handle, text.str().c_str(), L"Notepad info", MB_OK);
}


void COverlappedWindow::runProcesses()
{
	for (int i = 0; i < processesClearers.size(); ++i) {
		if (processesClearers[i].dwProcessId == 0) {
			STARTUPINFO si;
			ZeroMemory(&si, sizeof(si));
			if (!CreateProcess(L".\\dictionaryClearer.exe", 0, 0, 0, false, 0, 0, 0, &si, &processesClearers[i])) {
				MessageBox(0, L"Can't launch process", L"Error", MB_ICONERROR);
				continue;
			}
			std::wstring idstring = std::to_wstring(processesClearers[i].dwProcessId);
			std::wstring outputEventName = L"Global\\NotepadTextAvailible" + idstring;
			std::wstring inputEventName = L"Global\\NotepadTextCleared" + idstring;
			do {
				inputEvents[i] = ::OpenEvent(EVENT_ALL_ACCESS, false, inputEventName.c_str());
			} while (inputEvents[i] == 0);
			do {
				outputEvents[i] = ::OpenEvent(EVENT_ALL_ACCESS, false, outputEventName.c_str());
			} while (outputEvents[i] == 0);
		}
	}
}
