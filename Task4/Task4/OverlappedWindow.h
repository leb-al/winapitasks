#pragma once
#include "Settings.h"

class COverlappedWindow {
public:
	COverlappedWindow();
	~COverlappedWindow();

	static bool RegisterClass();

	bool Create();

	void Show(int cmdShow);

	HWND GetHandle();

	CSettings& GetSettings();

	static COverlappedWindow* GetWindowByHandle(HWND handle);

	void ApplySettings();

	HWND GetDialog();

protected:
	void OnDestroy();

private:
	HWND handle;
    HWND child;
    bool changes;
	HMODULE wordCountModule;

	static const wchar_t* ClassName;
	static const wchar_t* ErrorTitle;
	static const int ChidrenCount;

	static LRESULT __stdcall windowProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

	void onResize(const RECT* area);

    bool onClose();

	bool writeFile(bool result, HANDLE file, wchar_t * text, int len);

	bool save();

	bool open();

	void onCommandRecieved(WPARAM wParam, LPARAM lParam);

	int splitBySpace(wchar_t * buffer, int index, int length);

	void createMap(wchar_t * buffer, int position, int length, int processNum);

	void clearWords();

	LRESULT onCtlColorEdit(HWND handle, WPARAM wParam, LPARAM lParam);

	void showWordCount(int count);

	void runProcesses();

	CSettings settings;

	HWND dialog;

	HBRUSH brush;
	
	std::vector<PROCESS_INFORMATION> processesClearers;
	std::vector<HANDLE> inputEvents;
	std::vector<HANDLE> outputEvents;
};


