//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Task4.rc
//
#define IDC_MYICON                      2
#define IDD_TASK4_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TASK4                       107
#define IDC_TASK4                       109
#define IDR_MAINFRAME                   128
#define IDR_DEFAULTTEXT                 128
#define IDD_SETTING                     131
#define IDC_BUTTON1                     1000
#define IDC_BACKGOUNDBUTTON             1000
#define IDC_BUTTON2                     1001
#define IDC_FONTBUTTON                  1001
#define IDC_FONTSIZE                    1002
#define IDC_LABELSIZE                   1003
#define IDC_LABELOPACITY                1004
#define IDC_OPACITY                     1005
#define IDC_PREVIEW                     1006
#define IDB_OK                          1007
#define ID_FILE_SAVE                    32771
#define IDM_FILE_SAVE                   32772
#define IDA_QUIT                        32773
#define ID_VIEW_SETTINGS                32776
#define IDM_SETTING                     32777
#define ID_VIEW_SETTINGSNOTMODAL        32778
#define IDM_VIEW_SETTINGSNOTMODAL       32779
#define ID_EDIT_WORDCOUNT               32780
#define ID_                             32781
#define IDM_ORD_COUNT                   32782
#define IDM_WORD_COUNT                  32783
#define IDM_WORD_COUNT_DYNAMIC          32784
#define ID_FILE_OPEN                    32785
#define IDM_FILE_OPEN                   32786
#define ID_EDIT_DICTIONARYCLEAR         32787
#define IDM_EDIT_DICTIONARYCLEAR        32788
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
