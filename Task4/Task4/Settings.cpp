#include "stdafx.h"
#include "Settings.h"
#include "OverlappedWindow.h"
#include <Commdlg.h>

CSettings::CSettings()
{
	opacity = 255;
	fontsize = 18;
	background = RGB(255, 255, 255);
	foreground = RGB(0, 0, 0);
	preview = false;
}


CSettings::~CSettings()
{
}

COLORREF& CSettings::GetBackground()
{
	return background;
}

COLORREF & CSettings::GetForeground()
{
	return foreground;
}

BYTE CSettings::GetOpacity()
{
	return opacity;
}

int CSettings::GetFontSize()
{
	return fontsize;
}

INT_PTR __stdcall CSettings::DialogProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	CSettings& settings = COverlappedWindow::GetWindowByHandle(GetParent(handle))->GetSettings();
	switch (message) {
		case WM_INITDIALOG:
			settings.onInit(handle);
			break;
		case WM_CLOSE:
			settings.preview = false;
			EndDialog(handle, 0);
			break;
		case WM_COMMAND:
			return settings.onCommand(handle, wParam, lParam);
		case WM_HSCROLL:
			settings.onScroll(handle, wParam, lParam);
			break;
		default:
			return FALSE;
	}
	return TRUE;
}

void CSettings::onInit(HWND handle)
{
	LPARAM param = MAKELONG(0, 255);
	HWND trackbarOpacity = GetDlgItem(handle, IDC_OPACITY);
	SendMessage(trackbarOpacity, TBM_SETRANGE, TRUE, param);
	SendMessage(trackbarOpacity, TBM_SETPOS, TRUE, opacity);

	param = MAKELONG(8, 72);
	HWND trackbarFontSize = GetDlgItem(handle, IDC_FONTSIZE);
	SendMessage(trackbarFontSize, TBM_SETRANGE, TRUE, param);
	SendMessage(trackbarFontSize, TBM_SETPOS, TRUE, fontsize);

	oldBackground = background;
	oldForeground = foreground;
	oldOpacity = opacity;
	oldFontsize = fontsize;

	actualBackground = background;
	actualForeground = foreground;
	actualOpacity = opacity;
	actualFontsize = fontsize;
}

INT_PTR CSettings::onCommand(HWND handle, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam)) {
		case IDB_OK:
			actualize(handle);
			PostMessage(handle, WM_CLOSE, 0, 0);
			break;
		case IDCANCEL:
			restoreOldSettings(handle);
			PostMessage(handle, WM_CLOSE, 0, 0);
			break;
		case IDC_BACKGOUNDBUTTON:
			if (updateColor(handle, &actualBackground)) {
				update(handle);
			}
			break;	
		case IDC_FONTBUTTON:
				if (updateColor(handle, &actualForeground)) {
					update(handle);
				}
				break;
		case IDC_PREVIEW:
			onSetPreview(handle, wParam, lParam);
			break;
		default:
			return FALSE;
	}
	return TRUE;
}

bool CSettings::updateColor(HWND handle, COLORREF* color)
{
	CHOOSECOLOR chooseColor;
	static COLORREF customColors[16];

	ZeroMemory(&chooseColor, sizeof(chooseColor));
	chooseColor.lStructSize = sizeof(chooseColor);
	chooseColor.hwndOwner = handle;
	chooseColor.lpCustColors = reinterpret_cast<LPDWORD>(customColors);
	chooseColor.Flags = CC_FULLOPEN | CC_RGBINIT;
	chooseColor.rgbResult = *color;

	if (::ChooseColor(&chooseColor)) {
		*color = chooseColor.rgbResult;
		return true;
	}
	else {
		return false;
	}
}

void CSettings::update(HWND handle)
{
	if (preview) {
		actualize(handle);
	}
}

void CSettings::actualize(HWND handle)
{
	opacity = actualOpacity;
	fontsize = actualFontsize;
	background = actualBackground;
	foreground = actualForeground;
	applySettings(handle);
}

void CSettings::applySettings(HWND handle)
{
	COverlappedWindow::GetWindowByHandle(GetParent(handle))->ApplySettings();
}

void CSettings::onSetPreview(HWND handle, WPARAM wParam, LPARAM lParam)
{
	WORD param = HIWORD(wParam);
	preview = SendMessage(GetDlgItem(handle, IDC_PREVIEW), BM_GETCHECK, 0, 0);
	if (preview) {
		update(handle);
	}
	else {
		restoreOldSettings(handle);
	}
}

void CSettings::onScroll(HWND handle, WPARAM wParam, LPARAM lParam)
{
	actualOpacity = SendMessage(GetDlgItem(handle, IDC_OPACITY), TBM_GETPOS, 0, 0);
	actualFontsize = SendMessage(GetDlgItem(handle, IDC_FONTSIZE), TBM_GETPOS, 0, 0);
	update(handle);
}

void CSettings::restoreOldSettings(HWND handle)
{
	background = oldBackground;
	foreground = oldForeground;
	fontsize = oldFontsize;
	opacity = oldOpacity;
	applySettings(handle);
}



