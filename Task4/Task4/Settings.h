#pragma once
class CSettings {
public:
	CSettings();
	~CSettings();

	COLORREF& GetBackground();
	COLORREF& GetForeground();
	BYTE GetOpacity();
	int GetFontSize();

	static INT_PTR __stdcall DialogProc(HWND handle, UINT message, WPARAM wParam, LPARAM lParam);

private:
	void onInit(HWND handle);
	INT_PTR onCommand(HWND handle, WPARAM wParam, LPARAM lParam);
	bool updateColor(HWND handle, COLORREF* color);
	void update(HWND handle);
	void actualize(HWND handle);
	void applySettings(HWND handle);
	void onSetPreview(HWND handle, WPARAM wParam, LPARAM lParam);
	void onScroll(HWND handle, WPARAM wParam, LPARAM lParam);
	void restoreOldSettings(HWND handle);

	BYTE opacity;
	COLORREF background;
	COLORREF foreground;
	int fontsize;

	BYTE actualOpacity;
	COLORREF actualBackground;
	COLORREF actualForeground;
	int actualFontsize;

	BYTE oldOpacity;
	COLORREF oldBackground;
	COLORREF oldForeground;
	int oldFontsize;

	bool preview;
};

