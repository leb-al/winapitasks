// Task1Drawing.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Task4.h"
#include "OverlappedWindow.h"

int WINAPI wWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPWSTR    lpCmdLine,
	int       nCmdShow)
{
	if (!COverlappedWindow::RegisterClassW()) {
		return 1;
	}
	COverlappedWindow window;
	if (!window.Create()) {
		return 3;
	}
	window.Show(nCmdShow);


	HACCEL acceleartors = ::LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TASK4));
	MSG message;
	BOOL getMessageResult = 0;
	while ((getMessageResult = ::GetMessage(&message, 0, 0, 0)) != 0) {
		if (getMessageResult == -1) {
			return 2;
		}
		if (!::TranslateAccelerator(window.GetHandle(), acceleartors, &message) && !::IsDialogMessage(window.GetDialog(), &message)) {
			::TranslateMessage(&message);
			::DispatchMessage(&message);
		}
	}
	DestroyAcceleratorTable(acceleartors);

	return 0;
}



