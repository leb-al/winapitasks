#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             
#include <sstream>
#include <vector>
#include <windows.h>
#include <Commctrl.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <set>
#include <string>
#include <corecrt_wstring.h>
#include "resource.h"
