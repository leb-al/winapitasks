// dictionaryClearer.cpp: определяет точку входа для приложения.
//

#include "stdafx.h"
#include "dictionaryClearer.h"

static const wchar_t* ErrorTitle = L"Error";

std::wstring toUpper(std::wstring line)
{
	std::transform(line.begin(), line.end(), line.begin(), ::towupper);
	return line;
}



void loadDictionary(std::set<std::wstring>& dictionary)
{
	HMODULE instance = GetModuleHandle(0);
	HRSRC textResource = ::FindResource(instance, MAKEINTRESOURCE(IDR_DICT), L"WORDS");
	HGLOBAL textGlobal = ::LoadResource(instance, textResource);
	LPVOID text = ::LockResource(textGlobal);
	std::wstringstream stream(reinterpret_cast<wchar_t*>(text));
	std::wstring word;
	while (stream) {
		stream >> word;
		if (stream) {
			dictionary.insert(toUpper(word));
		}
	}
	FreeResource(textGlobal);
}

void clearFile(const std::set<std::wstring>& dictionary, HANDLE outputEvent)
{
	std::wstring filename = L"Local\\NotapadTextPart" + std::to_wstring(::GetCurrentProcessId());

	HANDLE file = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, filename.c_str());

	if (file == 0) {
		MessageBox(0, L"Can't open file. Can't create map", ErrorTitle, MB_ICONERROR);
		exit(1);
	}

	int* sizeBuffer = reinterpret_cast<int*> (MapViewOfFile(file, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(int)));
	if (sizeBuffer == 0) {
		MessageBox(0, L"Can't open file. Can't get size", ErrorTitle, MB_ICONERROR);
		exit(1);
	}
	int size = *sizeBuffer;
	wchar_t* buffer = reinterpret_cast<wchar_t*> (reinterpret_cast<char*>(
		MapViewOfFile(file, FILE_MAP_ALL_ACCESS, 0, 0, size)) + sizeof(int));
	if (buffer == 0) {
		MessageBox(0, L"Can't open file. Can't get content", ErrorTitle, MB_ICONERROR);
		exit(1);
	}
	std::wstringstream inStream(reinterpret_cast<wchar_t*>(buffer));
	MessageBox(0, reinterpret_cast<wchar_t*>(buffer), L"input", MB_OK);

	std::wstringstream outStream;
	std::wstring word;
	while (inStream) {
		inStream >> word;
		if (inStream) {
			if (dictionary.find(toUpper(word)) != dictionary.end()) {
				outStream << word << " ";
			}
		}
	}

	std::wstring result = outStream.str();
	const char* begin = reinterpret_cast<const char*>(result.c_str());
	const char* end = reinterpret_cast<const char*>(result.c_str() + result.length());
	size = end - begin;
	memcpy(buffer, begin, size);
	buffer[size] = 0;
	*sizeBuffer = size + sizeof(int) + 1;
	::SetEvent(outputEvent);
	
	MessageBox(0, result.c_str(), L"Result", MB_OK);
}



int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	std::set<std::wstring> dictionary;
	loadDictionary(dictionary);

	std::wstring idstring = std::to_wstring(::GetCurrentProcessId());
	std::wstring inputEventName = L"Global\\NotepadTextAvailible" + idstring;
	std::wstring outputEventName = L"Global\\NotepadTextCleared" + idstring;
	HANDLE finishEvent = ::CreateEvent(0, true, false, L"Global\\NotepadTerminate");
	HANDLE inputEvent = ::CreateEvent(0, false, false, inputEventName.c_str());
	HANDLE outputEvent = ::CreateEvent(0, false, false, outputEventName.c_str());
	HANDLE events[2];
	events[0] = inputEvent;
	events[1] = finishEvent;

	bool run = true;
	do {
		DWORD waitRes = ::WaitForMultipleObjects(2, events, FALSE, INFINITE);
		switch (waitRes) {
			case WAIT_OBJECT_0:
				clearFile(dictionary, outputEvent);
				SetEvent(outputEvent);
				break;
			case WAIT_OBJECT_0 + 1:
				CloseHandle(inputEvent);
				CloseHandle(outputEvent);
				CloseHandle(finishEvent);
				exit(0);
		}
	} while (1);
	return 0;
}

