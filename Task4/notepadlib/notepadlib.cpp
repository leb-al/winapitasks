// notepadlib.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include "notepadlib.h"

NOTEPADLIB_API int WordsCount(const wchar_t * text)
{
	int lastSpace = -1;
	int count = 0;
	const wchar_t * symbol = text;
	while (*symbol != 0) {
		if (iswspace(*symbol)) {
			int index = symbol - text;
			if ((lastSpace + 1) != index) {
				count++;
			}
			lastSpace = index;
		}
		++symbol;
	}
	if ((symbol - text) != (lastSpace + 1)) {
		++count;
	}
	return count;
}
